package com.dd.tan.entity;

public class Accessory {
	public int id;
	public 	String name;
	public Accessory(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Accessory [id=" + id + ", name=" + name + "]";
	}
}
