package com.dd.tan.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Product {
	int id;
	String name;
	public int categoryId;
	public LocalDate saleDate;
	public int qulity;
	public boolean isDelete;

	public Product(int id, String name, int categoryId, LocalDate saleDate, int qulity, boolean isDelete) {
		this.id = id;
		this.name = name;
		this.categoryId = categoryId;
		this.saleDate = saleDate;
		this.qulity = qulity;
		this.isDelete = isDelete;
	}

	public Product(int id, String name) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "product [id=" + id + ", name=" + name + "]";
	}

	public static List<Product> getProducts() {
		List<Product> results = new ArrayList<Product>();
		return results;
	}
}
