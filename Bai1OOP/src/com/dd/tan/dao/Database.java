package com.dd.tan.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dd.tan.entity.Product;

public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public Database Instances;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();
	}

	public int insertTable(String name, Object row) {
		int i = 0;
		if (name.equals("product")) {
			this.productTable.add(row);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.add(row);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.add(row);
			i = 1;
		}
		return i;
	}

	public ArrayList<Object> selectTable(String name) {

		if (name.equals("product")) {
			return this.productTable;
		}
		if (name.equals("category")) {
			return this.categoryTable;

		}
		if (name.equals("accessory")) {
			return this.accessoryTable;

		}
		return null;
	}

	// ham update

	public ArrayList<Object> updateTable(String name) {

		if (name.equals("product")) {
			return this.productTable;
		}
		if (name.equals("category")) {
			return this.categoryTable;

		}
		if (name.equals("accessory")) {
			return this.accessoryTable;

		}
		return null;
	}

	// delete
	public Boolean deleteTable(String name, Object row) {
		Boolean check = true;
		if (name.equals("product")) {
			this.productTable.remove(row);
		}
		if (name.equals("category")) {
			return this.categoryTable.remove(row);
		}
		if (name.equals("accessory")) {
			this.accessoryTable.remove(row);

		}
		return check;
	}

	//
	public void truncateTable(String name) {

		if (name.equals("product")) {
			this.productTable.clear();
			System.out.println("TrucateProduct");
		}
		if (name.equals("category")) {
			this.productTable.clear();
			System.out.println("TrucateCategory");
		}
		if (name.equals("accessory")) {
			this.productTable.clear();
			System.out.println("TrucateAccessory");

		}

	}

	public Integer updateTable(String name, Object oldRow, Object newRow) {
		int i = 0;
		ArrayList<Object> objProduct = selectTable("product");
		ArrayList<Object> objCategory = selectTable("category");
		ArrayList<Object> objAccessory = selectTable("accessory");
		Integer objproduct = objProduct.indexOf(oldRow);
		Integer objcategory = objCategory.indexOf(oldRow);
		Integer objaccessory = objAccessory.indexOf(oldRow);
		
		if (name.equals("product")) {
			this.productTable.set(objproduct, newRow);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.set(objcategory, newRow);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.set(objaccessory, newRow);
			i = 1;
		}
		return i;
	}
}
