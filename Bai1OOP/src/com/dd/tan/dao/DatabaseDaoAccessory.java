package com.dd.tan.dao;

import java.lang.reflect.Array;
import java.util.List;

import com.dd.tan.entity.Accessory;

public interface DatabaseDaoAccessory {
	public List<Accessory> getAll();
	public int insertTable(int name, int row);
	public Array selectTable(Array name);
	public int updateTable(int name, int row);
	public boolean deleteTable(boolean name, boolean row);
	public int insertTable(String name);
}
