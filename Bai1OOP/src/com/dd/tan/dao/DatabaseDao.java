package com.dd.tan.dao;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public interface DatabaseDao {
	public int insertTable(String name, Object row);
	public Arrays selectTable(Array name);
	public int updateTable(int name, int row);
	public boolean deleteTable(boolean name, boolean row);
	public int insertTable(String name);
	public List<Database> getAll();
}
