package com.dd.tan.entity;

import java.util.ArrayList;
import java.util.List;

public class Accessory {
	public int id;
	public 	String name;
	public Accessory(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Accessory [id=" + id + ", name=" + name + "]";
	}
	
	public static List<Accessory> getAccessory() {
		List<Accessory> results = new ArrayList<Accessory>();
		results.add(new Accessory(1, "A"));
		results.add(new Accessory(2, "B"));
		results.add(new Accessory(3, "C"));
		results.add(new Accessory(4, "D"));
		results.add(new Accessory(5, "E"));
		results.add(new Accessory(6, "F"));
		results.add(new Accessory(7, "G"));
		results.add(new Accessory(8, "H"));
		results.add(new Accessory(9, "J"));
		return results;
	}

}
