package com.dd.tan.entity;

import java.util.ArrayList;
import java.util.List;

public class Category {
	public int id;
	public String name;
	
	public Category(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}
	public static List<Category> getCategory() {
		List<Category> results = new ArrayList<Category>();
		results.add(new Category(1, "AA"));
		results.add(new Category(2, "BB"));
		results.add(new Category(3, "CC"));
		results.add(new Category(4, "DD"));
		results.add(new Category(5, "EE"));
		results.add(new Category(6, "FF"));
		results.add(new Category(7, "GG"));
		results.add(new Category(8, "HH"));
		results.add(new Category(9, "JJ"));
		return results;
	}

}
