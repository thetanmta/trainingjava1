package com.dd.tan.entity;

import java.util.ArrayList;
import java.util.List;

public class Product {
	 public int id;
	  public String name;
	  public String categoryID;
	  public int categoryId;
	  public int qulity;
	  public Boolean isDelete;

	public Product(int id, String name, int categoryId, int qulity, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.qulity = qulity;
        this.isDelete = isDelete;
    }

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", categoryid=" + categoryID + "]";
	}
	public static List<Product> getProducts() {
		List<Product> results = new ArrayList<Product>();
		return results;
	}

}
