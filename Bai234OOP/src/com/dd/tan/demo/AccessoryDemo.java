package com.dd.tan.demo;

import java.util.ArrayList;

import com.dd.tan.dao.Database;
import com.dd.tan.entity.Accessory;

public class AccessoryDemo {
	static Database database = new Database();

    public static void createCategoryTest() {

        ArrayList<Accessory> accessories = new ArrayList<Accessory>();
        accessories.add(new Accessory(1, "Đồ ăn"));
        accessories.add(new Accessory(2, "Đồ ăn"));
        accessories.add(new Accessory(3, "Đồ uống"));
        accessories.add(new Accessory(4, "Đồ uống"));
        accessories.add(new Accessory(5, "Đồ uống"));
        accessories.add(new Accessory(6, "Đồ uống"));
        accessories.add(new Accessory(7, "Đồ uống"));
        accessories.add(new Accessory(8, "Đồ uống"));
        accessories.add(new Accessory(9, "Đồ uống"));
        accessories.add(new Accessory(10, "Đồ uống"));
        System.out.println(accessories);
    }
    private static void printCategory(){

        System.out.println("in");
        System.out.println(new Accessory(10, "Đồ uống"));
    }

    public static void main(String[] args) {

        createCategoryTest();
        printCategory();
    }

}
