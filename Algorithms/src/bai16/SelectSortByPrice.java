package bai16;

import java.util.ArrayList;
import java.util.List;

import bai12.Product;

public class SelectSortByPrice {
	static ArrayList<Product> listProducts = Product.addProductList();

	public static void main(String[] args) {
		selecSortByPrice(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}

	public static List<Product> selecSortByPrice(List<Product> listProduct) {
		int i, j;
		for (i = 0; i < listProduct.size()-1; i++) {
			int min = i;
			for (j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(j).getPrice() < listProduct.get(min).getPrice()) {
					min = j;
				}
			}
				Product temp = listProduct.get(i);
				listProduct.set(i, listProduct.get(min));
				listProduct.set(min, temp);
		}
		return listProducts;
	}
}
