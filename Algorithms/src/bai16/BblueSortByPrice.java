package bai16;

import java.util.ArrayList;
import java.util.List;
import bai12.Product;

public class BblueSortByPrice {
	static ArrayList<Product> listProducts = Product.addProductList();

	public static void main(String[] args) {
		List<Product> listProductSortByPrice = sortByPrice(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}

	}

	public static List<Product> sortByPrice(List<Product> listProduct) {
		int i, j;
		for (i = 0; i < listProduct.size() - 1; i++) {
			for (j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getPrice() >listProduct.get(j).getPrice()) {
					Product temp = listProduct.get(j);
					listProduct.set(j, listProduct.get(i));
					listProduct.set(i, temp);
				}
			}
		}
		return listProducts;
	}
}
