package bai16;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class InsertSortByPrice {
	static ArrayList<Product> listProducts = Product.addProductList();
	public static void main(String[] args) {
		List<Product> listProduct = sortByPrice(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}

	public static List<Product> sortByPrice(List<Product> listProduct) {
		for (int i = 1; i < listProduct.size(); i++) {
			Product temp = listProduct.get(i);
			int j = i -1;
			while((j > -1) && (listProduct.get(j).getPrice()>temp.getPrice())) {
				listProduct.set(j+1,listProduct.get(j));
				j = j -1;
			}
			listProduct.set(j+1, temp);
		}
		return listProduct;
	}
}
