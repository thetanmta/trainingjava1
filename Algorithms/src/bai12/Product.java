package bai12;

import java.util.ArrayList;
import java.util.List;

public class Product {
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuality() {
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public Product(String name,int price,int quality,int categoryId) {
		super();
		this.name=name;
		this.price=price;
		this.quality=quality;
		this.categoryId=categoryId;
		
	}
	public Product() {
		super();
	}
	private String categoryName;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	private String name;
	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", quality=" + quality + ", categoryId=" + categoryId
				+ ",categoryName="+categoryName+"]";
	}
	
	public int compareToPrice(Product pro) {
		return this.getPrice()-pro.getPrice();
	}
	private int price;
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	private int quality;
	private int categoryId;
	public static ArrayList<Product> addProductList() {
		ArrayList<Product> listProduct = new ArrayList<>();
		listProduct.add(new Product("CPU", 750, 10, 1));
		listProduct.add(new Product("RAM", 50, 2, 2));
		listProduct.add(new Product("HDD", 70, 1, 2));
		listProduct.add(new Product("Main", 400, 3, 1));
		listProduct.add(new Product("Keyboard", 30, 8, 4));
		listProduct.add(new Product("Mouse", 25, 50, 4));
		listProduct.add(new Product("VGA", 60, 35, 3));
		listProduct.add(new Product("Monitor", 120, 28, 2));

		System.out.println(listProduct);
		return listProduct;
	}

}
