package bai12;

import java.awt.image.ConvolveOp;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListProduct {
	static List<Product> list = new LinkedList<>();
	static List<Category> listCategory = new LinkedList<>();
	static Product pro = new Product();

	public static void main(String[] args) {
		System.out.println("danh sach product");
		listProducts();
		System.out.println("danh sac category");
		listCategory();
		System.out.println("tim kiem product co ten CPU");
		findProduct(list, "CPU");
		System.out.println("tim product co category = 2");
		findProducByCategory(list, 2);
		System.out.println("tim cac san pham co gia nho hon 300");
		findProductPrice(list, 300);
		System.out.println("----------------");
		mapProducByCategory(list, listCategory);
		System.out.println("sap xep theo gia");
		sortByPrice(list);
		System.out.println("sắp xếp theo tên category");
		sortByCategoryName(list);
		// System.out.println("sap xep theo do dai ten");
		// sortByName(list);
		for (Product product : list) {
			System.out.println(product);
		}
		System.out.println("caSalary  khong su dung de quy " + caSalary(10, 3));

		double caSalaryUsingRecursion = caSalaryUsingRecursion(10, 3);
		System.out.println("salary su dung de quy :" + caSalaryUsingRecursion);
		int month = calMonth(100, 0.2);
		System.out.println("dem thang khong su dung de quy " + month);
		System.out.println("dem thang: " + calMontUsingRecursion(100, 0.2));
	}

	

	private static void listCategory() {
		Category Category1 = new Category(1, "Comuter");
		listCategory.add(Category1);
		Category Category2 = new Category(2, "Memory");
		listCategory.add(Category2);
		Category Category3 = new Category(3, "Card");
		listCategory.add(Category3);
		Category Category4 = new Category(4, "Accessory");
		listCategory.add(Category4);
		for (Category student : listCategory) {
			System.out.println(student);
		}
		
	}



	public static void listProducts() {
		Product product1 = new Product("CPU", 750, 10, 1);
		list.add(product1);
		Product product2 = new Product("RAM", 50, 2, 2);
		list.add(product2);
		Product product3 = new Product("HDD", 70, 1, 2);
		list.add(product3);
		Product product4 = new Product("Main", 400, 5, 1);
		list.add(product4);
		Product product5 = new Product("Keyboard", 30, 9, 4);
		list.add(product5);
		Product product6 = new Product("Mouse", 25, 8, 4);
		list.add(product6);
		Product product7 = new Product("VGA", 60, 7, 1);
		list.add(product7);
		for (Product product : list) {
			System.out.println(product);
		}
	}

	public static void findProduct(List<Product> list, String nameProduct) {
		for (Product pro : list) {
			if (nameProduct.equals(pro.getName().toString())) {
				System.out.println(nameProduct);
			}
		}
	}

	public static List<Product> findProducByCategory(List<Product> list, int categoryId) {
		List<Product> products = new LinkedList<Product>();
		try {

			for (Product product : list) {
				if (product.getCategoryId() == categoryId) {
					products.add(product);
					System.out.println(product);
				}
			}

		} catch (Exception e) {
		}
		return products;
	}

	public static List<Product> mapProducByCategory(List<Product> list, List<Category> listCategory) {
		List<Product> products = new LinkedList<Product>();
		// List<Category> categorys = new LinkedList<Category>();
		for (Product product : list) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
					products.add(product);
					System.out.println(product);
				}

			}
		}
		return products;
	}

	public static List<Product> findProductPrice(List<Product> list, int price) {
		List<Product> products = new LinkedList<Product>();
		try {
			for (Product product : list) {
				if (product.getPrice() < price) {
					products.add(product);
					System.out.println(product);
				}
			}
		} catch (Exception e) {
		}
		return products;
	}

	public static List<Product> sortByPrice(List<Product> listProduct) {
		List<Product> products = new LinkedList<Product>();
		for (int i = 0; i < listProduct.size() - 1; i++) {
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getPrice() > listProduct.get(j).getPrice()) {
					Product temp = listProduct.get(j);
					listProduct.set(j, listProduct.get(i));
					listProduct.set(i, temp);
				}
			}
		}
		return products;

	}

	// cau 17 sap xep theo tên
	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = new LinkedList<Product>();
		for (int i = 0; i < listProduct.size() - 1; i++) {
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getCategoryName().compareTo(listProduct.get(j).getCategoryName()) > 0) {
					Product temp = listProduct.get(i);
					listProduct.set(i, listProduct.get(j));
					listProduct.set(j, temp);
				}
			}
		}
		return products;
	}

	// sap xep theo ten tu cao xong thap
	public static List<Product> sortByName(List<Product> listProduct) {
		List<Product> products = new LinkedList<Product>();
		for (int i = 0; i < listProduct.size() - 1; i++) {
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(i).getName().length() < listProduct.get(j).getName().length()) {
					Product temp = listProduct.get(i);
					listProduct.set(i, listProduct.get(j));
					listProduct.set(j, temp);
				}
			}
		}
		return products;
	}

	public static double caSalary(double salary, int n) {
		double result = salary;
		for (int i = 2; i <= n; i++) {
			result += result * 0.2;
		}
		return result;
	}

	public static double caSalaryUsingRecursion(double salary, int n) { // n=5 int result = 0;
		if (n <= 1) {
			return salary;
		} else {
			return caSalaryUsingRecursion(salary *1.2, (n - 1));
		}
	}

	public static int calMonth(double money, double rate) {
		int month = 0;
		int result = 0;
		while (result <= (2 * money)) {
			month++;
			result += month * rate;
		}
		return month;
	}

	public static int calMontUsingRecursion(double money, double rate) {
		int result = 0;
		int month =0;
		if ((result += month * rate) >= (2 * money)) {
			return month;
		} else {
			month++;
			return calMontUsingRecursion((result += month * rate), rate);
		}
	}
}
