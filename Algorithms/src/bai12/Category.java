package bai12;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Category {
	private int id;
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}

	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Category(int id,String name) {
		super();
		this.id=id;
		this.name=name;
	}
	public Category() {
		super();
	}
	public static ArrayList<Category> addCategory() {
		ArrayList<Category> results = new ArrayList<Category>();
		results.add(new Category( 1, "Comuter"));
		results.add(new Category(2, "Memory"));
		results.add(new Category(3, "Card"));
		results.add(new Category(4, "Accessory"));
		return results;
	}

	public static void listCategory() {
	
	}
	



}
