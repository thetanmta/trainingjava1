package bai11;

import bai10.Stack;

public class Queue {
	private static int maxSize = 8;
	private static int[] queueArray = new int[maxSize];
	private static int front = 0;
	private static int rear = -1;
	private static int itemCount = 0;

	static int peek() {
		return queueArray[front];
	}

	public static boolean isEmpty() {
		return itemCount == 0;
	}

	public static boolean isFull() {
		return itemCount == maxSize;
	}

	int size() {
		return itemCount;
	}

	static void push(int data) {
		if (!isFull()) {
			if (rear == maxSize - 1) {
				rear = -1;
			}
			queueArray[++rear] = data;
			itemCount++;
		}
	}

	static int removeData() {
		int data = queueArray[front++];

		if (front == maxSize) {
			front = 0;
		}

		itemCount--;
		return data;
	}

	public static void main(String[] args) {
		push(3);
		push(5);
		push(9);
		push(1);
		push(12);
		push(15);

		if (isFull()) {
			System.out.println("Hang doi (Queue) da day!\n");
		}

		// xoa mot phan tu
		int num = removeData();
		System.out.print("Phan tu bi xoa: " + num);
		push(16);
		push(17);
		push(18);
		int dau = peek();
		System.out.println("\nphan tu vao dau tien cua queue la: "+dau);
		System.out.println("\nHang doi (Queue):  ");

		while (!isEmpty()) {
			int n = removeData();
			System.out.println(" " + n);
		}
	}
}
