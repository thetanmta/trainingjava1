package bai8;

public class DeQuy {
	 static int count = 0;
	    static void welcome() {
	        count++;
	        if (count <= 5) { // Phần cơ sở: Điều kiện thoát khỏi đệ quy
	            System.out.println(count + "Xin chao cac ban ");
	            welcome(); // Phần đệ quy: Thân hàm có chứa lời gọi đệ quy
	        }
	    }
	  
	    public static void main(String[] args) {
	        welcome();
	    }

}
