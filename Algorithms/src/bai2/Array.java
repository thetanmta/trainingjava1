package bai2;

import java.util.Scanner;

public class Array {
	public static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int c;
		int n;
		int[] arr;

		System.out.print("Nhập số phần tử của mảng: ");
		n = scanner.nextInt();
		arr = new int[n];
		imput(arr, n);
		// khởi tạo arr
		n = arr.length;
		System.out.print("Nhập phần tử k = ");
		int k = scanner.nextInt();
		// sắp xếp dãy số theo thứ tự tăng dần
		sortASC(arr);
		System.out.print("Sắp xếp mảng tăng dần: ");
		show(arr);
		System.out.printf("\nChèn phần tử %d vào mảng.", k);
		arr = insert(arr, k);
		System.out.print("\nMảng sau khi chèn: ");
		show(arr);
		System.out.print("Nhập gia tri can xoa k = ");
		int key = scanner.nextInt();
		System.out.print("\nMảng sau khi xoa la: ");
		deleteElement(arr, n, key);
		show(arr);
		System.out.print("Nhập gia tri can tim kiem: ");
		int x = scanner.nextInt();
		if (linearSearch(arr, x)) {
			System.out.println("ton tai x trong mang");
		} else {
			System.out.println("khong ton tai x trong mang");
		}
	}

	public static void imput(int[] arr, int n) {

		System.out.print("Nhập các phần tử của mảng: \n");
		for (int i = 0; i < arr.length; i++) {
			System.out.printf("a[%d] = ", i);
			arr[i] = scanner.nextInt();
		}
	}
	public static void show(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

	public static void sortASC(int[] arr) {
		int temp = arr[0];
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
				}
			}
		}
	}

	public static int[] insert(int[] arr, int k) {
		int arrIndex = arr.length - 1;
		int tempIndex = arr.length;
		int[] tempArr = new int[tempIndex + 1];
		boolean inserted = false;

		for (int i = tempIndex; i >= 0; i--) {
			if (arrIndex > -1 && arr[arrIndex] > k) {
				tempArr[i] = arr[arrIndex--];
			} else {
				if (!inserted) {
					tempArr[i] = k;
					inserted = true;
				} else {
					tempArr[i] = arr[arrIndex--];
				}
			}
		}
		return tempArr;
	}

	// xoa phan tu trong mang
	public static void deleteElement(int arr[], int n, int key) {
		int d = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == key) {
				for (int j = 0; j < arr.length - 1; j++)
					arr[j] = arr[j + 1];
				n--;
				d++;
			}
			if (arr[n] == key)
				n--;
			if (d == 0)
				System.out.println("khong co gia tri can xoa trong mang");
			if (n == 0)
				System.out.println("mang da xoa hoan toan");
		}
	}

	// tim kiem phan tu
	static boolean linearSearch(int arr[], int x) {
		int i;
		for (i = 0; i <= arr.length; i++)
			if (arr[i] == x)
				return true;
		return false;
	}
	void bubbleSort(int x[], int N) {
		for (int i = 0; i < x.length - 1; i++) {
			for (int j = N - 1; j > i; j--) {
				if (x[j] < x[j - 1]) {
					int tg = x[j];
					x[j] = x[j - 1];
					x[j - 1] = tg;
				}
			}
		}
	}

	void selection(int arr[]) {
		for (int i = 0; i < arr.length-1; i++) {
			int m = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[m] > arr[j])
					m = j;
			}
			if (m != i) {
				int tam = arr[m];
				arr[m] = arr[i];
				arr[i] = tam;
			}
		}
	}

	void insertionSort(int x[]) {
		for (int i = 1; i < x.length; i++) {
			int j = i - 1;
			int t = x[i];
			while (t < x[j] && j >= 0) {
				x[j + 1] = x[j];
				j--;
			}
			x[j + 1] = t;
		}
	}
}
