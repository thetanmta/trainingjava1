package bai21;

import bai12.Product;

public class Queue {
	private static int maxSize = 8;
	private static Object[] queueArray = new Object[maxSize];
	private static int front = 0;
	private static int rear = -1;
	private static int itemCount = 0;

	static Object peek() {
		return queueArray[front];
	}

	public static boolean isEmpty() {
		return itemCount == 0;
	}

	public static boolean isFull() {
		return itemCount == maxSize;
	}

	int size() {
		return itemCount;
	}

	static void push(Object data) {
		if (!isFull()) {
			if (rear == maxSize - 1) {
				rear = -1;
			}
			queueArray[++rear] = data;
			itemCount++;
		}
	}

	static Object get() {
		Object data = queueArray[front++];

		if (front == maxSize) {
			front = 0;
		}

		itemCount--;
		return data;
	}

	public static void main(String[] args) {
		push(new Product("CPU", 750, 10, 1));
		push(new Product("CPU", 750, 10, 1));
		push(new Product("RAM", 50, 2, 2));
		push(new Product("Main", 400, 3, 1));
		push(new Product("Keyboard", 30, 8, 4));
		push(new Product("Mouse", 25, 50, 4));
		push(new Product("Mouse", 25, 50, 4));

		if (isFull()) {
			System.out.println("Hang doi (Queue) da day!\n");
		}

		// xoa mot phan tu
		Object num = get();
		System.out.print("Phan tu bi xoa: " + num);
		push(new Product("Keyboard", 30, 8, 4));
		push(new Product("Mouse", 25, 50, 4));
		push(new Product("Mouse", 25, 50, 4));
		Object dau = peek();
		System.out.println("\nphan tu vao dau tien cua queue la: "+dau);
		System.out.println("\nHang doi (Queue):  ");

		while (!isEmpty()) {
			Object n = get();
			System.out.println(" " + n);
		}
	}
}
