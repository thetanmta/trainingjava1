package Bai22;

import bai12.Product;

public class Stack {
	private int maxSize;
	private Object[] stackArray;
	private int top;

	public Stack(int s) {
		maxSize = s;
		stackArray = new Product[maxSize];
		top = -1;
	}

	public void push(Object j) throws Exception {
		stackArray[++top] = j;
	}

	public Object get() {
		return stackArray[top--];
	}

	public Object peek() {
		return stackArray[top];
	}

	public boolean isEmpty() {
		return (top == -1);
	}

	public boolean isFull() {
		return (top == maxSize - 1);
	}
	//in cac phan tu cua stack
	public void Output(Stack S)
	{
	    for (int i=S.top; i>=0; i--)
	    	System.out.println(" "+stackArray[i]);
	}

	public static void main(String[] args) throws Exception {
		Stack theStack = new Stack(10);
		theStack.push(new Product("CPU", 750, 10, 1));
		theStack.push(new Product("RAM", 50, 2, 2));
		theStack.push(new Product("Main", 400, 3, 1));
		theStack.push(new Product("Keyboard", 30, 8, 4));
		theStack.push(new Product("Mouse", 25, 50, 4));
		theStack.push(new Product("Mouse", 25, 50, 4));
		
//		while (!theStack.isEmpty()) {
//			long value = theStack.pop();
//			System.out.print(value);
//			System.out.print(" ");
//		}
		System.out.println("Phan tu cua stack l�");
		theStack.Output(theStack);
		Object value = theStack.get();
		System.out.println("phan tu dau cua stack l�: "+value);
		theStack.Output(theStack);
	}

}
