package bai18;

import java.util.ArrayList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class InsertSort {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategoys = Category.addCategory();

	public static void main(String[] args) {
		mapProductByCategory(listProducts, listCategoys);
		List<Product> listProduct = sortByCategoryName(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}

	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		for (int i = 1; i < listProduct.size(); i++) {
			Product temp = listProduct.get(i);
			int j = i -1;
			while((j > -1) && (listProduct.get(j).getName().compareTo(temp.getCategoryName())>0)) {
				listProduct.set(j+1,listProduct.get(j));
				j = j -1;
			}
			listProduct.set(j+1, temp);
		}
		return listProduct;
	}
	
	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}

}
