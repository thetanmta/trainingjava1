package bai18;

import java.util.ArrayList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class SelectSort {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategoys = Category.addCategory();

	public static void main(String[] args) {
		mapProductByCategory(listProducts, listCategoys);
		List<Product> listProduct = sortByCategoryName(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}

	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(min).getCategoryName().compareTo(listProduct.get(j).getCategoryName()) > 0) {
					System.out.println();
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			listProduct.set(i, listProduct.get(min));
			listProduct.set(min, temp);
		}
		return listProduct;
	}

	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}

}
