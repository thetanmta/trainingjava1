package bai18;

import java.util.ArrayList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class BblueSort {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategoys = Category.addCategory();
	
	public static void main(String[] args) {
		mapProductByCategory(listProducts,listCategoys);
		List<Product> listProduct = sortByCategoryName(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}
	
	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for(int i =0;i<listProduct.size();i++) {
			for(int j = 1;j<listProduct.size()-i;j++) {
				if(products.get(j-1).getCategoryName().compareTo(products.get(j).getCategoryName())>0) {
					Product temp = listProduct.get(j-1);
					products.set(j-1, listProduct.get(j));
					products.set(j, temp);
				}
			}
		}
		return products;
	}
	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}


}
