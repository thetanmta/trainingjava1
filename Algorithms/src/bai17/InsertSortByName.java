package bai17;

import java.util.ArrayList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class InsertSortByName {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategoys = Category.addCategory();

	public static void main(String[] args) {
		List<Product> listProduct = sortByName(listProducts);
		System.out.println(listProduct);
	}

	public static List<Product> sortByName(List<Product> listProduct) {
		for (int i = 1; i < listProduct.size(); i++) {
			Product temp = listProduct.get(i);
			int j = i -1;
			while((j > -1) && (listProduct.get(j).getName().length()>temp.getName().length())) {
				listProduct.set(j+1,listProduct.get(j));
				j = j -1;
			}
			listProduct.set(j+1, temp);
		}
		return listProduct;
	}
}
