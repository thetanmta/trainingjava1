package bai17;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class BblueSortByName {
	static ArrayList<Product> listProducts = Product.addProductList();
	static ArrayList<Category> listCategoys = Category.addCategory();
	
	public static void main(String[] args) {
		sortByName(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}
	
	public static List<Product> sortByName(List<Product> listProduct) {
		for(int i =0;i<listProduct.size();i++) {
			for(int j = 1;j<listProduct.size()-i;j++) {
				if(listProduct.get(j-1).getName().length()>listProduct.get(j).getName().length()) {
					Product temp = listProduct.get(j-1);
					listProduct.set(j-1, listProduct.get(j));
					listProduct.set(j, temp);
				}
			}
		}
		return listProduct;
	}

}
