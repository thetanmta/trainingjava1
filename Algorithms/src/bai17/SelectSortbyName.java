package bai17;

import java.util.ArrayList;
import java.util.List;

import bai12.Category;
import bai12.Product;

public class SelectSortbyName {
	static ArrayList<Product> listProducts = Product.addProductList();
	

	public static void main(String[] args) {
		List<Product> listProduct = sortByName(listProducts);
		for (Product product : listProducts) {
			System.out.println(product);
		}
	}

	public static List<Product> sortByName(List<Product> listProduct) {
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(j).getName().length() < listProduct.get(min).getName().length()) {
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			listProduct.set(i, listProduct.get(min));
			listProduct.set(min, temp);
		}
		return listProduct;
	}

}
