import java.util.stream.Stream;
public class DefaultMethod1 implements DefaultMethod{
	
	public static void main(String[] args){
		DefaultMethod1 tiger = new DefaultMethod1() ; 
        tiger.move();
        
        // su dung foreach
        Stream.iterate(1, count -> count + 1) //
        .filter(number -> number % 3 == 0) //
        .limit(6) //
        .forEach(System.out::println);
	}
}
