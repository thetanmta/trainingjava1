import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.lang.reflect.Member;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Stream {

	public static void main(String[] args) {
		// Tạo Stream cho những kiểu primitive
		IntStream.range(1, 4).forEach(System.out::print); // 1 2 3
		IntStream.of(1, 2, 3).forEach(System.out::print); // 1 2 3
		DoubleStream.of(1, 2, 3).forEach(System.out::print); // 1.0 2.0 3.0
		LongStream.range(1, 4).forEach(System.out::print); // 1 2 3
		LongStream.of(1, 2, 3).forEach(System.out::print); // 1 2 3

		// stream su dung map
		List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
		data.stream().map(String::toUpperCase) // chuyen doi tung doi tuong sang chu viet hoa
				.forEach(System.out::println);

		// stream du dung store de sap xep
		List<String> data1 = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
		data1.stream().sorted().forEach(System.out::println);
		data1.stream().sorted((s1, s2) -> s1.length() - s2.length()) 
				.forEach(System.out::println);
		
		// stream dung filter()
		List<String> nemberNames = new ArrayList<String>();
		nemberNames.add("AnhTan");
		nemberNames.add("Hau");
		nemberNames.add("Tuan 767 o9u9uo ");
		nemberNames.add("Hoang jhjhjhj .kjn,m. ");
		nemberNames.add("Khai");
		nemberNames.add("Hung");
		nemberNames.stream().filter((s) -> s.startsWith("A")).forEach(System.out::println);

		// stream.collection
		List<String> memNamesInUppercase = nemberNames.stream().sorted().map(String::toUpperCase)
				.collect(Collectors.toList());
		System.out.print(memNamesInUppercase);

		// stream.match()
		boolean matchedResult = nemberNames.stream().anyMatch((s) -> s.startsWith("A"));
		System.out.println(matchedResult);
		matchedResult = nemberNames.stream().allMatch((s) -> s.startsWith("A"));
		System.out.println(matchedResult);
		matchedResult = nemberNames.stream().noneMatch((s) -> s.startsWith("A"));

		// stream.count()
		long totalMatched = nemberNames.stream().filter((s) -> s.startsWith("A")).count();
		System.out.println(totalMatched);
		
	}

}
