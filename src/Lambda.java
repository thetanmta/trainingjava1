import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@FunctionalInterface

// ham lambda su dung nhieu tham so
interface Addable {
	public int add(int a, int b);
}

// ham labda khong su dung tham so
interface Addable1 {
	public int add();
}

public class Lambda {
	public static void main(String[] args) {
		/*
		 * // Một danh sách các loại quả. List<String> fruits =
		 * Arrays.asList("Grapefruit", "Apple", "Durian", "Cherry");
		 * 
		 * // Sử dụng phương thức tiện ích của Collections // để sắp xếp lại danh sách
		 * trên. // Cung cấp một Comparator (Bộ so sách). Collections.sort(fruits, new
		 * Comparator<String>() {
		 * 
		 * @Override public int compare(String o1, String o2) { return o1.compareTo(o2);
		 * }
		 * 
		 * });
		 * 
		 * for (String fruit : fruits) { System.out.println(fruit); }
		 */
		// in ra 1 sanh các phần tử đã có trong mảng
		List<String> pointList = new ArrayList();
		pointList.add("1");
		pointList.add("2");
		pointList.add("9");
		pointList.add("7");
		pointList.forEach(p -> {
			System.out.println(p);
		});

		// thuc hien cong hai so dungf bieu thuc lambda
		Addable ad1 = (int a, int b) -> (a + b);
		System.out.println(ad1.add(10, 20));

		// bieu thuc lambda tao thread
		Runnable r1 = new Runnable() {
			public void run() {
				System.out.println("Runnable 1");
			}
		};
		Runnable r2 = () -> System.out.println("Runnable 2");

		r1.run();
		r2.run();

		// sap xep theo ten
		Employee[] employees = { new Employee("David"), new Employee("Naveen"), new Employee("Alex"),
				new Employee("Richard") };

		System.out.println("Before Sorting Names: " + Arrays.toString(employees));
		Arrays.sort(employees, Employee::nameCompare);
		System.out.println("After Sorting Names " + Arrays.toString(employees));
		//
		
	}
}
