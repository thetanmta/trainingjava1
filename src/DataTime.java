import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DataTime {

	public static void main(String[] args) {
		/*// ngay hien tai
		LocalDate today = LocalDate.now();
		System.out.println("thoi gian hien tai la:" +today);
		LocalDate firstDay_2019 = LocalDate.of(2019, Month.JANUARY, 01);
		System.out.println("Specific date =" +firstDay_2019);
		LocalDate todayHn = LocalDate.now(ZoneId.of("Asia/Ha_Noi"));
		System.out.println(" thoi gian hien tai la:"+todayHn);
        LocalDate dateFromBase = LocalDate.ofEpochDay(365);
        System.out.println("365th day from base date = " + dateFromBase);
        // Obtains an instance of LocalDate from a year and day-of-year
        LocalDate hundredDay2014 = LocalDate.ofYearDay(2014, 100);
        System.out.println("100th day of 2014 = " + hundredDay2014);
        
        */
        //chuyen doi qua lai giua cac kieu thoi gian
        // LocalDate/ LocalTime <-> LocalDateTime/ ZonedDateTime
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        LocalDateTime dateTimeFromDateAndTime = LocalDateTime.of(date, time);
        LocalDate dateFromDateTime = dateTimeFromDateAndTime.toLocalDate();
        LocalTime timeFromDateTime = dateTimeFromDateAndTime.toLocalTime();
        ZonedDateTime hcmDateTime = ZonedDateTime.of(dateTimeFromDateAndTime, ZoneId.of("Asia/Ho_Chi_Minh"));
        // Convert old classes to Java 8 Date Time
        Instant instantFromDate = new Date().toInstant();
        ZoneId zoneId = TimeZone.getDefault().toZoneId();
        Instant instantFromCalendar = Calendar.getInstance().toInstant();
        ZonedDateTime zonedDateTime = new GregorianCalendar().toZonedDateTime();
 
        // Instant <-> LocalDateTime
        Instant instant = Instant.now();
        LocalDateTime dateTimeFromInstant = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        Instant instantFromLocalDateTime = dateTimeFromInstant.toInstant(ZoneOffset.ofHours(+7));
 
        // Instant <-> LocalDate
        LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        Instant instantFromLocalDate = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
 
        // Convert Java 8 Date Time to old classes
        Date dateFromInstant = Date.from(Instant.now());
        TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of("Asia/Ho_Chi_Minh"));
        GregorianCalendar gregorianCalendar = GregorianCalendar.from(ZonedDateTime.now());
		

		// dateTime API
		 LocalDate today = LocalDate.now(); 
	        // Get the Year, check if it's leap year
	        System.out.println("Year " + today.getYear() + " is Leap Year? " + today.isLeapYear());
	        // Compare two LocalDate for before and after
	        System.out.println("Today is before 01/01/2018? " + today.isBefore(LocalDate.of(2018, 1, 1)));
	        // Create LocalDateTime from LocalDate
	        System.out.println("Current Time = " + today.atTime(LocalTime.now()));	 
	        // plus and minus operations
	        System.out.println("10 days after today will be " + today.plusDays(10));
	        System.out.println("3 weeks after today will be " + today.plusWeeks(3));
	        System.out.println("20 months after today will be " + today.plusMonths(20));
	        System.out.println("10 days before today will be " + today.minusDays(10));
	        System.out.println("3 weeks before today will be " + today.minusWeeks(3));
	        System.out.println("20 months before today will be " + today.minusMonths(20));	 
	        // Temporal adjusters for adjusting the dates
	        LocalDate firstDayOfThisMonth = today.with(TemporalAdjusters.firstDayOfMonth());
	        System.out.println("First date of this month= " + firstDayOfThisMonth);         
	        LocalDate lastDayOfThisMonth = today.with(TemporalAdjusters.lastDayOfMonth());
	        System.out.println("Last date of this month= " + lastDayOfThisMonth);         
	        LocalDate lastDayOfYear = today.with(TemporalAdjusters.lastDayOfYear());
	        System.out.println("Last date of this year= " + lastDayOfYear); 
	        Period period = today.until(lastDayOfYear);
	        System.out.println("Period Format= " + period);
	        System.out.println("Months remaining in the year= " + period.getMonths());

	}

}
