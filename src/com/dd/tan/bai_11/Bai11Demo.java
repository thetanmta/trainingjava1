package com.dd.tan.bai_11;
import java.util.List;

import com.dd.tan.bai_09.Product;;
public class Bai11Demo {
	public static void main(String[] args) {
		List<Product> listProducts = Product.getProduct();
		Product result1 = Bai11.filterProductById(listProducts, 07);
		System.out.println(result1.getId() + " " + result1.getName());
		Product result2 = Bai11Stream.filterProductById(listProducts,07);
		System.out.println("Stream : " + result2.getId() + " " + result2.getName());

	}
}
