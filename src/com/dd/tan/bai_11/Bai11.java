package com.dd.tan.bai_11;

import java.util.List;

import com.dd.tan.bai_09.Product;

public class Bai11 {
	public static Product filterProductById(List<Product> listProduct, int id) {
		Product result = null;
		for (Product temp : listProduct) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}
}
