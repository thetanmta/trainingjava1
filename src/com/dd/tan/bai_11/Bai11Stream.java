package com.dd.tan.bai_11;

import com.dd.tan.bai_09.Product;
import java.util.List;

public class Bai11Stream {
	public static Product filterProductById(List<Product> listProducts, int id) {
		Product result2 = listProducts.stream().filter(product -> id == product.getId()).findAny().orElse(null);
		return result2;
	}

}
