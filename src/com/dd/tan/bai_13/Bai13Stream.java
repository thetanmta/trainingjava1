package com.dd.tan.bai_13;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.dd.tan.bai_09.Product;

public class Bai13Stream {
	public static List<Product> filterProductBySaleDate(List<Product> listProducts) {
		List<Product> products = listProducts.stream()
				.filter(product -> product.isDelete() == false && product.getSaleDate().isAfter(LocalDate.now()))
				.collect(Collectors.toList());
		return products;
	}

}
