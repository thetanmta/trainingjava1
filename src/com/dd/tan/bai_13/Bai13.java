package com.dd.tan.bai_13;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dd.tan.bai_09.Product;

public class Bai13 {
	public static List<Product> filterProductBySaleDate(List<com.dd.tan.bai_09.Product> listProducts){
		List<Product> products = new ArrayList<Product>();
		for(Product product : listProducts) {
			if(product.getSaleDate().isAfter(LocalDate.now())&&product.isDelete()==false) {
				products.add(product);
			}
		}
		return products;
		
	}

}
