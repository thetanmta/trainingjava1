package com.dd.tan.bai_13;

import java.time.LocalDate;
import java.util.List;

import com.dd.tan.bai_09.Product;
public class Bai13Demo {

	public static void main(String[] args) {
		List<Product> listProducts = Product.getProduct();
		List<Product> products = Bai13.filterProductBySaleDate(listProducts);
		System.out.println(products);

		//dung stream
		List<Product> products2 = Bai13Stream.filterProductBySaleDate(listProducts);
		System.out.println("Stream : "+products2);

	}
	

}
