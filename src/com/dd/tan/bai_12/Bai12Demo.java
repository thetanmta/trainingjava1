package com.dd.tan.bai_12;

import java.util.List;
import java.util.stream.Collectors;

import com.dd.tan.bai_12.Bai12;
import com.dd.tan.bai_11.Bai11Stream;
import com.dd.tan.bai_09.Product;

public class Bai12Demo {

	public static void main(String[] args) {
		List<Product> listProduct = Product.getProduct();
		List<Product> products = Bai12.fiterProductByQulity(listProduct, true);
		System.out.println(products);
		
		// dung stream
		List<Product> products2 = Bai12Stream.fiterProductByQulity(listProduct);
		System.out.println("Stream : " + products2);

	}

}
