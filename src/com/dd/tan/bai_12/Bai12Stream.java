package com.dd.tan.bai_12;

import java.util.List;
import java.util.stream.Collectors;

import com.dd.tan.bai_09.Product;

public class Bai12Stream {
	public static List<Product> fiterProductByQulity(List<Product> listProducts) {
		List<Product> result = listProducts.stream().filter(item->item.isDelete()==false && item.getQulity()>0).collect(Collectors.toList());
		result.forEach(System.out::println);
		return  result;
	}
}
