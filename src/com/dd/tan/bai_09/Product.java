package com.dd.tan.bai_09;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Product {
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public int getQulity() {
		return qulity;
	}
	
	public void setQulity(int qulity) {
		this.qulity = qulity;
	}
	
	public boolean isDelete() {
		return isDelete;
	}
	
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	private int id;
	private String name;
	private int categoryId;
	private LocalDate saleDate;
	public LocalDate getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(LocalDate saleDate) {
		this.saleDate = saleDate;
	}
	private int qulity;
	private boolean isDelete;
	public Product(int id,String name, int categoryId, LocalDate saleDate,int qulity, Boolean isDelete) {
		this.id=id;
		this.name= name;
		this.categoryId=categoryId;
		this.saleDate=saleDate;
		this.qulity=qulity;
		this.isDelete=isDelete;
	}
	
	public String toString() {
		return	name;
	}
	public static List<Product> getProduct() {
		List<Product> products = new ArrayList<Product>();
		LocalDate today = LocalDate.now();
		LocalDate previousWeek = today.minus(1, ChronoUnit.WEEKS);
		LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
		try {
			products.add(new Product(01, "May loc nuoc", 14, nextWeek, 0, true));
			products.add(new Product(02, "Lo vi song", 19, nextWeek, 9, false));
			products.add(new Product(03, "tu lanh", 19, today, 9, true));
			products.add(new Product(04, "Ti vi", 19, nextWeek, 9, false));
			products.add(new Product(05, "May loc nuoc", 19, previousWeek, 9, true));
			products.add(new Product(06, "Ca phe", 19, nextWeek, 9, false));
			products.add(new Product(07, "May tinh", 19, nextWeek, 9, true));
			products.add(new Product(10, "May loc nuoc", 19, nextWeek, 0, false));
			products.add(new Product(11, "May loc nuoc", 19, nextWeek, 9, true));
			products.add(new Product(12, "May loc nuoc", 19, today, 9, false));
			products.add(new Product(8, "May loc nuoc", 0, nextWeek, 9, true));
			products.add(new Product(9, "May loc nuoc", 0, previousWeek, 0, true));
			return products;
		} catch (Exception e) {
			System.out.println(e);
		}
		return products;

	}

	

	
}
