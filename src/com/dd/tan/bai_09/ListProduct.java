package com.dd.tan.bai_09;

import java.util.ArrayList;
import java.util.List;
import java.io.ObjectInputStream.GetField;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes.Name;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.lang.model.util.SimpleAnnotationValueVisitor6;

import com.dd.tan.bai_07.Person;
import com.dd.tan.bai_09.Product;;

public class ListProduct {
	

	public static void main(String[] args) {
		//showListProcduct(); 
		usingStream();
			
		//tim kiem san pham theo id dung stream
		List<Product> listProduct = Product.getProduct();
		Product result = fiterProductById(listProduct, 07);
		System.out.println(result.getName());
		Product result1 =listProduct.stream().filter(item->07==(item.getId())).findAny().orElse(null);
		System.out.println("san pham can tim la:"+result1);
		
		//tim kiem san pham theo id khong dung stream
		System.out.println(result.getName()+" "+result.getId());
		
		// in ra ds sp cos chat luong lon hon 0 va chua bi xoa
		List<Product> result2 = fiterProductByQulity(listProduct, false);
		System.out.println("danh sach qulity >0:"+result2);
		
		//dung stream
		List<Product> result4 = listProduct.stream().filter(item->item.isDelete()==false && item.getQulity()>0).collect(Collectors.toList());
		result4.forEach(System.out::println);
		
		
		
		// bai 13 
		LocalDate today = LocalDate.now();
		System.out.println("Today is before 01/01/2018? " + today.isBefore(LocalDate.of(2018, 1, 1)));
        List<Product> result6 = listProduct.stream().filter(product -> product.isDelete() == false && product.getSaleDate().isAfter(LocalDate.now())).collect(Collectors.toList());
        System.out.println("Dùng stream : "+result6);
        
        //stream reduce bai 14
        List<Product> result7 = listProduct.stream().filter(product -> product.isDelete() == false).collect(Collectors.toList());
        int resultReduce =result7.stream().reduce(0 ,(a,b) ->a + b.getQulity() ,Integer::sum);
        System.out.println(resultReduce);
        
        //bai 15
        boolean result9 = isHaveProductInCategory(listProduct, 3);
        System.out.println(result9);
        
        // bai 16 
	     System.out.println("Today is before 01/01/2018? " + today.isBefore(LocalDate.of(2018, 1, 1)));
	     List<Product> result8 = listProduct.stream().filter(product -> product.isDelete() == false &&product.getQulity()>0 && product.getSaleDate().isAfter(LocalDate.now())).collect(Collectors.toList());
	     System.out.println("Dùng stream : "+result8);
	}

	// hien thi ten cac san pham dung lambda
	public static void showListProcduct() {
		List<Product> listProduct = Product.getProduct();
		System.out.println("Hien thi ra ten cac san pham");
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		listProduct.forEach(p -> {
			System.out.println(p.getName());

		});

	}

	// show ra danh sach san pham dung stream
	public static void usingStream() {
		List<Product> listProduct = Product.getProduct();
		System.out.println("Hien thi ra ten cac san pham");
		listProduct.stream().forEach(p -> {
			System.out.println(p.getName());
		});
	}

	// show ra san pham tim kiem theo Id
	private static Product fiterProductById(List<Product> products, int id) {
		Product result = null;
		for (Product temp : products) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}

	private static List<Product> fiterProductByQulity(List<Product> products, Boolean isDelete) {
		List<Product> result = new ArrayList<Product>();
		for (Product temp : products) {
			if (isDelete == temp.isDelete() && temp.getQulity() > 0) {
				result.add(temp);
			}
		}
		return result;
	}

	// bai 13 dua ra ds sp có ngày sale lớn hơn ngay hien tai va chua bi xoa
	private static List<Product> fiterProductBySaleDate(List<Product> products, Date date) {
		List<Product> result = new ArrayList<Product>();
		LocalDate today = LocalDate.now();
		for (Product temp : products) {
			if (temp.getSaleDate().isAfter(LocalDate.now()) && temp.isDelete() == false) {
				result.add(temp);
			}
		}
		return result;
	}
	
	//bai 16
	private static List<Product> fiterProductBySaleDate16(List<Product> products, Date date) {
		List<Product> result = new ArrayList<Product>();
		LocalDate today = LocalDate.now();
		for (Product temp : products) {
			if (temp.getSaleDate().isAfter(LocalDate.now()) && temp.isDelete() == false && temp.getQulity()>0) {
				result.add(temp);
			}
		}
		return result;
	}
	
	// Bai 15 
	private static boolean isHaveProductInCategory(List<Product> listProducts,int categoryID){
        Boolean result = null;
        for(Product temp : listProducts) {
            if(categoryID==temp.getCategoryId()) {
                result = true;
            }
            else{
                result = false;
            }
        }
        return result;
    }

	
	

}
