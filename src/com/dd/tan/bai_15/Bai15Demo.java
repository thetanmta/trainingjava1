package com.dd.tan.bai_15;
import com.dd.tan.bai_09.Product;
import java.util.List;
public class Bai15Demo {
	public static void main(String[] args) {
		List<Product> listProducts = Product.getProduct();
		
		boolean checkCategoryId = Bai15.isHaveProductInCategory(listProducts,4);
		System.out.println(checkCategoryId);
		
		boolean checkCategoryId2 = Bai15Stream.isHaveProductInCategory(listProducts, 10);
		System.out.println("Steam : "+checkCategoryId2);
	}

}
