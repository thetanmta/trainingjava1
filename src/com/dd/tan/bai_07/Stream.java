package com.dd.tan.bai_07;

import java.util.List;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;


public class Stream {

	public static void main(String[] args) {
		List<Person> persons =Arrays.asList(
				new Person("The Tan",30),
				new Person("Nguyen Dai",30),
				new Person("Minh Chi",30),
				new Person("Ha Vu",30)
				);
		
		Person result = getStudentByName(persons, "The Tan");
		System.out.println(result.getName());
		
		// d�ng filter, map, findAny, orElse
		Person result1 =persons.stream().filter(x->"The Tan".equals(x.getName())).findAny().orElse(null);
		
		
	}
	
	private static Person getStudentByName(List<Person> persons ,String name) {
		Person result = null;
		for(Person temp: persons) {
			if(name.equals(temp.getName())) {
				result =temp;
			}
		}
		return result;
		
	}
	

}
