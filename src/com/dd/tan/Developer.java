package com.dd.tan;

import java.math.BigDecimal;

public class Developer {
	private String name;
	//private float salary;
	private BigDecimal salary;
	
	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
	private int age;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public Developer(String name, BigDecimal salary, int age) {
		this.name=name;
		this.salary=salary;
		this.age=age;	
	}
	public String toString() {
		return	name;
	}
	public static int nameCompare(Developer a1, Developer a2) {
		    return a1.name.compareTo(a2.name);
	}
	
	
}
