package com.dd.tan.bai_14;

import java.util.List;

import com.dd.tan.bai_09.Product;

public class Bai14Demo {

	public static void main(String[] args) {
		List<Product> listProducts = Product.getProduct();

		Integer totalQuility = Bai14.totalProduct(listProducts);
		System.out.println(totalQuility);
		
		// dung stream
		Integer totalQuility2 = Bai14Stream.totalProduct(listProducts);
		System.out.println("Stream : " + totalQuility2);

	}

}
