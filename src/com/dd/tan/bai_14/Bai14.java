package com.dd.tan.bai_14;
import java.util.List;

import com.dd.tan.bai_09.Product;


public class Bai14 {
	public static Integer totalProduct(List<Product> listProducts) {
		int totalProduct = 0;
		for(Product product : listProducts) {
			if(product.isDelete()==false) {
				totalProduct = totalProduct + product.getQulity();
			}
		}
		return totalProduct;
	}

}
