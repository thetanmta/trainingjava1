package com.dd.tan.bai_14;
import java.util.List;
import java.util.stream.Collectors;

import com.dd.tan.bai_09.Product;
public class Bai14Stream {
	public static Integer totalProduct(List<Product> listProducts) {
		List<Product> products = listProducts.stream().filter(product -> product.isDelete() == false).collect(Collectors.toList());
		int totalQuility =products.stream().reduce(0 ,(a,b) ->a + b.getQulity() ,Integer::sum);
		return totalQuility;
	}

}
