package com.dd.tan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Bai_6 {
	
	private static List<Developer> getDevelopers() {

		List<Developer> result = new ArrayList<Developer>();

		result.add(new Developer("Tan",new BigDecimal("500000"),23));
		result.add(new Developer("Hien",new BigDecimal("90000") ,22));
		result.add(new Developer("Hanh",new BigDecimal("30000"),21));
		result.add(new Developer("Hue",new BigDecimal("80000"),20));
		result.add(new Developer("Huyen",new BigDecimal("20000"),24));
		result.add(new Developer("Linh",new BigDecimal("40000"),21));
		return result;

	}
	public static void main(String[] args) {
		// hien thi ra danh sach 
		List<Developer> listDeveloper = getDevelopers();
		System.out.println("danh sach truoc khi sap xep");
		for(Developer developer: listDeveloper) {
			System.out.println(developer.getName()+" "+developer.getSalary()+" "+developer.getAge());
		}
		
		//sap xep danh sach theo tuoi
		Collections.sort(listDeveloper, new Comparator<Developer>() {
			@Override
			public int compare(Developer o1, Developer o2) {
				return o1.getAge() - o2.getAge();
			}
		});
		
		//sap xep tuoi theo lambda
		listDeveloper.sort((Developer a, Developer b)->a.getAge()-b.getAge());			
		
		//sap xep theo ten 
		Collections.sort(listDeveloper,new Comparator<Developer>() {
			@Override
			public int compare(Developer o1, Developer o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		//sap xep theo ten dung ham lambda
		Comparator<Developer> byName = (Developer a, Developer b) -> a.getName().compareTo(b.getName());
				
		/*/sap xep theo luong dung float
		Collections.sort(listDeveloper,new Comparator<Developer>() {
			@Override
			public int compare(Developer a, Developer b) {
				
				if( a.getSalary() > b.getSalary()) {
					return 1;
				}
				if( a.getSalary() < b.getSalary()) {
					return -1;
				}	
				return 0;		
			}
		});
		*/
		
		//sap xep theo luong 
		Collections.sort(listDeveloper, new Comparator<Developer>() {
			@Override
			public int compare(Developer a , Developer b) {
				return a.getSalary().compareTo(b.getSalary());
			}
		});
		
		//sap xep luong theo kieu lambda
		listDeveloper.sort((a ,b)->a.getSalary().compareTo(b.getSalary()));
		
		
		//danh sach sau khi sap xep
		System.out.println("danh sach sau khi sap xep");
		for(Developer developer: listDeveloper) {
			System.out.println(developer.getName()+" "+developer.getSalary()+" "+developer.getAge());
		}
		
	}

	
}
