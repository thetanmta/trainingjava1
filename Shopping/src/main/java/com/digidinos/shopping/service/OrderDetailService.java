package com.digidinos.shopping.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.responsitory.OrderDetailReponsitory;

@Service
public class OrderDetailService {

	@Autowired
	private OrderDetailReponsitory repo;
	
	public OrderDetailService() {
		// TODO Auto-generated constructor stub
	}
	
	public Optional<OrderDetail> selectById(int id) {
		Optional<OrderDetail> orderDetail = repo.findById(id);
		return orderDetail;
	}
	
	public List<OrderDetail> findAllByOrderId(Integer orderId){
		List<OrderDetail> listOrderDetail = repo.findAllByOrderId(orderId);
		return listOrderDetail;
	}

}
