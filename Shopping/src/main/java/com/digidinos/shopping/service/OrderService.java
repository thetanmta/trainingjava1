package com.digidinos.shopping.service;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.model.CartInfo;
import com.digidinos.shopping.model.CartLineInfo;
import com.digidinos.shopping.model.CustomerInfo;
import com.digidinos.shopping.responsitory.OrderDetailReponsitory;
import com.digidinos.shopping.responsitory.OrderReponsitory;
import com.digidinos.shopping.responsitory.ProductReponsitory;

@Service
public class OrderService {
	@Autowired
	private OrderReponsitory repositoryOrder;
	@Autowired
	private ProductReponsitory repositoryProduct;
	@Autowired
	private OrderDetailReponsitory repositoryOrderDetail;

	public List<Order> findAll() {
		List<Order> orders = repositoryOrder.findAll();
		return orders;
	}
	public Page<Order> findAll(Pageable pageable){
		return repositoryOrder.findAll(pageable);
	}



	public Optional<Order> findById(int id) {
		Optional<Order> order = repositoryOrder.findById(id);
		return order;
	}

	public void addOrder(CartInfo cartInfo) {
		int orderNum = repositoryOrder.maxOrderNum() + 1;
		System.out.println(orderNum);
		Order order = new Order();
		order.setOrderNum(orderNum);
		order.setOrderDate(new Date());
		order.setAmount(cartInfo.getAmountTotal());
		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerAddress(customerInfo.getAddress());
		System.out.println("Name : " + order.getCustomerName());
		repositoryOrder.save(order);
		List<CartLineInfo> lines = cartInfo.getCartLines();
		for (CartLineInfo line : lines) {
			OrderDetail detail = new OrderDetail();
			detail.setOrder(order);
			detail.setAmount(line.getAmount());
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setQuanity(line.getQuantity());
			int id = line.getProductInfo().getId();
			Optional<Product> productOpt = repositoryProduct.findById(id);
			Product product = productOpt.get();
			detail.setProduct(product);
			repositoryOrderDetail.save(detail);
		}
	}
}
