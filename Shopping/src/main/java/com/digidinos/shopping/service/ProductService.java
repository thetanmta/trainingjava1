package com.digidinos.shopping.service;

import com.digidinos.shopping.entity.BaseEntity;
import com.digidinos.shopping.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import com.digidinos.shopping.responsitory.ProductReponsitory;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
	@Autowired
	private ProductReponsitory repo;
	public List<Product> top5NewProduct(){
		List<Product> products = repo.top5ProductNew();
		return products;
	}
	

	public Page<Product> findAll(Pageable pageable){
		return repo.findAll(pageable);
	}

	public List<Product> findNameProuct() {
		return repo.findByName();
	}
	public List<Product> selectAll() {
		List<Product> products = repo.findAll();
		return products;
	}

	public Optional<Product> selectById(int id) {
		return repo.findById(id);
	}

	public List<Product> selectByPrice(int size) {
		return repo.findByPrice(size);
	}
	

	public void addReservation(Product product) {
		repo.save(product);
	}

	public void updateReservation(Product product) {
		repo.save(product);
	}


	public void deleteReservation(int id) {
		Optional<Product> productOpt = selectById(id);
		Product product = productOpt.get();
		product.setDelete(true);
		product.setDeletedAt(new Date());
		repo.save(product);
	}
	public Page<Product> findByNamePage(String name,Pageable pageable){
		return repo.findByNamePage(name,pageable);
	}

}
