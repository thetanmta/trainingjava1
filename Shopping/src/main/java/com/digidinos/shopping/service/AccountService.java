package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.responsitory.AccountReponsitory;

@Service
public class AccountService {
	@Autowired
	AccountReponsitory accountRepository;

	public List<Account> selectAll() {
		return this.accountRepository.findAll();
	}
	public AccountService() {
		// TODO Auto-generated constructor stub
	}

	public Optional<Account> selectById(int id) {
		Optional<Account> accountOpt = accountRepository.findById(id);
		return accountOpt;
	}

	public void add(Account account) {
		accountRepository.save(account);

	}

	public Page<Account> findAll(Pageable pageable) {
		return this.accountRepository.findAll(pageable);
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		this.accountRepository.deleteById(id);
	}

	public Optional<Account> selectbyid(int id) {

		return accountRepository.findById(id);

	}
	


	public Account findByUserName(String userName) {
		Account account = accountRepository.findByUserName(userName);
		return account;
	}

	public void updatePassword(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setEncrytedPassword(account.getEncrytedPassword());
		}
		this.accountRepository.save(account2);
	}

	public void updateProfile(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setImage(account.getImage());
		}
		this.accountRepository.save(account2);
	}

	public void update(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setUserName(account.getUserName());
			account2.setUserRole(account.getUserRole());
		}
		this.accountRepository.save(account2);
	}

	public void updateInfo(Account account) {
		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setImage(account2.getImage());
		}
		this.accountRepository.save(account2);
	}

	public void addReservation(Account account) {
		accountRepository.save(account);
	}
	public Page<Account> findByAccountName(String name,Pageable pageable){
		return accountRepository.findByNameAccount(name, pageable);
	}


}
