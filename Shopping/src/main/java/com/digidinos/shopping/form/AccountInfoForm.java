package com.digidinos.shopping.form;
import java.io.IOException;
import java.util.Date;

import javax.persistence.Id;

import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shopping.entity.Account;


public class AccountInfoForm {
	@Id
	private int id;
	private String userName;
	//private String address;
	private String userRole;
	private boolean newAccount = false;
	private byte[] image;
	//private String desception;
//	public String getDesception() {
//		return desception;
//	}
//
//	public void setDesception(String desception) {
//		this.desception = desception;
//	}

	// Upload file.
	private MultipartFile fileData;

	public AccountInfoForm() {
		// TODO Auto-generated constructor stub
		this.newAccount = true;
	}

	public AccountInfoForm(Account account) {
		this.id = account.getId();
		this.userName = account.getUserName();
		this.userRole = account.getUserRole();
		//this.address = account.getAddress();
		//this.desception = account.getDesception();

		this.image = account.getImage();
	}
	
	public Account getAccount() throws IOException {
		Account account = new Account();
		account.setId(this.id);
		account.setUserRole(this.userRole);
		account.setUserName(this.userName);
		account.setImage(this.fileData.getBytes());
		account.setUpdatedAt(new Date());
		return account;	
	}
	
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public MultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(MultipartFile fileData) {
		this.fileData = fileData;
	}

	public boolean isNewAccount() {
		return newAccount;
	}

	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
