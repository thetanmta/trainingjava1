package com.digidinos.shopping.form;

import java.io.IOException;
import java.util.Date;
import org.springframework.web.multipart.MultipartFile;
import com.digidinos.shopping.entity.Product;

public class ProductForm {
	private int id;
	private String code;
	private String name;
	private double price;
	private boolean isDelete;
	private boolean newProduct = false;
	private byte[] image;

	// Upload file.
	private MultipartFile fileData;

	public ProductForm() {
		this.newProduct = true;
	}

	public ProductForm(Product product) {
		this.id = product.getId();
		this.code = product.getCode();
		this.name = product.getName();
		this.price = product.getPrice();
	}

	public Product getProduct() throws IOException {
		Product product = new Product();
		product.setId(this.id);
		product.setCode(this.code);
		product.setName(this.name);
		product.setPrice(this.price);
		product.setDelete(false);
		product.setCreatedAt(new Date());
		product.setImage(this.getFileData().getBytes());
		return product;
	}

	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public MultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(MultipartFile fileData) {
		this.fileData = fileData;
	}

	public boolean isNewProduct() {
		return newProduct;
	}

	public void setNewProduct(boolean newProduct) {
		this.newProduct = newProduct;
	}

}
