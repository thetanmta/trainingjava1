package com.digidinos.shopping.responsitory;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.digidinos.shopping.entity.Account;

@Repository
public interface AccountReponsitory extends BaseReponsitory<Account, Integer> {
	@Override
	default List<Account> findAll() {
		return this.findAll();
	}

	Account findByUserName(String userName);

	@Query(value = "select * FROM accounts WHERE user_name LIKE  %:name%", nativeQuery = true)
	Page<Account> findByNameAccount(@Param("name") String name, Pageable pageable);
}
