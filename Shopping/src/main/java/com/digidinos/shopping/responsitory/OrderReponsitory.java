package com.digidinos.shopping.responsitory;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;


import com.digidinos.shopping.entity.Order;
@Repository
public interface OrderReponsitory  extends BaseReponsitory<Order, Integer>{

	@Query(value="select max(o.order_num) from Orders o", nativeQuery = true)
	int maxOrderNum();
	
	
	@Query(value="SELECT * FROM Orders o where o.id = :orderId", nativeQuery = true) 
	Order findOrderInfoByID(int orderId);
	@Query(value = "select * FROM orders WHERE CUSTOMER_NAME LIKE  %:name%", nativeQuery = true)
	Page<Order> findByCustomerName(@Param("name") String name,Pageable pageable);


}
