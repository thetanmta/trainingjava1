package com.digidinos.shopping.responsitory;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import com.digidinos.shopping.entity.Product;

@Repository
public interface ProductReponsitory extends BaseReponsitory<Product, Integer> {
	@Query(value = "select t.* FROM products t where t.price = :price", nativeQuery = true)
	List<Product> findByPrice(@Param("price") int price);

	@Query(value="select  id, image, created_at , deleted_at , updated_at , code , is_delete , name , price, description from products where is_delete = 0 order by created_at desc limit 5", nativeQuery = true) 
	List<Product> top5ProductNew();

	
	// tim kiem ten san pham
	@Query(value = "select t.* FROM products t where t.name LIKE :name", nativeQuery = true)
	List<Product> findByName();
	
	//@Query(value="Select p.* from Products p where p.name like %:name% and is_delete = 0",nativeQuery = true)
	//Page<Product> findByNamePage(@Param("name") String name,Pageable pageable);
	@Query(value="select * FROM products WHERE NAME LIKE  %:name% and is_delete=0",nativeQuery = true)
	Page<Product> findByNamePage(@Param("name") String name,Pageable pageable);
	
	@Query(value="Select * from Products where is_delete=0",nativeQuery = true)
	Page<Product> findAll(Pageable pageable);


}
