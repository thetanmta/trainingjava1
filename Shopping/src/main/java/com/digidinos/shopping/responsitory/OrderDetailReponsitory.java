package com.digidinos.shopping.responsitory;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.thymeleaf.standard.expression.OrExpression;

import com.digidinos.shopping.entity.OrderDetail;

@Repository
public interface OrderDetailReponsitory extends BaseReponsitory<OrderDetail, Integer> {
	@Query(value = "SELECT * FROM Order_Details o where o.order_id = :orderId", nativeQuery = true)
	List<OrderDetail> findAllByOrderId(Integer orderId);

	

	

}
