package com.digidinos.shopping.responsitory;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.digidinos.shopping.entity.BaseEntity;
@NoRepositoryBean
public interface BaseReponsitory<T extends BaseEntity,ID extends Serializable> extends JpaRepository<T , ID> {
	
}
