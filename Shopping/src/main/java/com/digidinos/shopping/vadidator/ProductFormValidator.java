package com.digidinos.shopping.vadidator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.ProductForm;
 
@Component

public class ProductFormValidator implements Validator{

	  @Override
	    public boolean supports(Class<?> clazz) {
	        return clazz == ProductForm.class;
	    }

		@Override
		public void validate(Object target, Errors errors) {
			ProductForm productForm = (ProductForm) target;

			// Kiểm tra các trường (field) của ProductForm.
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "NotEmpty.productForm.code");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.productForm.name");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty.productForm.price");
			if(productForm.getFileData().isEmpty()) {
				errors.rejectValue("fileData", "NotEmpty.productForm.fileData");
			}

			
		}

}
