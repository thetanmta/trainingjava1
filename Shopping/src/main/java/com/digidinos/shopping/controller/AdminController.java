package com.digidinos.shopping.controller;

import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.AccountChangePasswordForm;
import com.digidinos.shopping.form.AccountChangeRoleForm;
import com.digidinos.shopping.form.AccountForm;
import com.digidinos.shopping.form.AccountInfoForm;
import com.digidinos.shopping.form.AddAccountForm;
import com.digidinos.shopping.form.ProductForm;

import com.digidinos.shopping.model.OrderDetailInfo;
import com.digidinos.shopping.model.OrderInfo;
import com.digidinos.shopping.model.ProductInfo;
import com.digidinos.shopping.pagination.Pagination;
import com.digidinos.shopping.responsitory.ProductReponsitory;
import com.digidinos.shopping.service.OrderDetailService;
import com.digidinos.shopping.service.OrderService;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.AddAccountService;
import com.digidinos.shopping.vadidator.AccountChangePasswordValidator;
import com.digidinos.shopping.vadidator.AccountInfoValidator;
import com.digidinos.shopping.vadidator.AddAccountValidator;
import com.digidinos.shopping.vadidator.ProductFormValidator;

@Controller
@Transactional
public class AdminController {
	@Autowired
	private AccountService accountService1;

	@Autowired
	private OrderService orderService;
	@Autowired
	private ProductService productService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProductFormValidator productFormValidator;

	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private AddAccountValidator addAccountValidator;
	
	@Autowired
	private AccountInfoValidator accountInfoValidator;

	@Autowired
	private Pagination pagination;
	@Autowired
	private AccountChangePasswordValidator accountFormValidator;


	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == ProductForm.class) {
			///
			dataBinder.setValidator(productFormValidator);
		}
		
		if (target.getClass() == AccountInfoForm.class) {
			
			dataBinder.setValidator(accountInfoValidator);
			System.out.println("Account Info Validator Valid");
		}
		if (target.getClass() == AccountChangePasswordForm.class) {
			dataBinder.setValidator(accountFormValidator);
		}
		if (target.getClass() == AddAccountForm.class) {
			dataBinder.setValidator(addAccountValidator);
		}


	}

	// GET: Hiển thị trang login
	@RequestMapping(value = { "/admin/login" }, method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}

	// lay ra cac san pham
	@RequestMapping(value = { "/admin/productManager" }, method = RequestMethod.GET)
	public String listProductHandler(Model model, 
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "3") int size) {

		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> pageProduct = productService.findByNamePage(likeName, pageable);
			pagingNumber = pagination.paginationProduct(pageProduct, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationProducts", pageProduct);
			return "productManager";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationProducts", productPage);
		return "productManager";

	}

	// lay ra danh sach user
	@RequestMapping(value = { "/admin/user" }, method = RequestMethod.GET)
	public String user(Model model) {
		List<Account> result = accountService.selectAll();
		model.addAttribute("paginationProducts", accountService.selectAll());
		return "productManager";
	}

	
	// lay danh sach san pham
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.GET)
	public String product(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				Product product = productOpt.get();
				productForm = new ProductForm(product);
			}
		}
		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "product";
	}

	// POST: Save product
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.POST)
	public String productSave(Model model, @ModelAttribute("productForm") @Validated ProductForm productForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "product";
		}
		try {
			productService.addReservation(productForm.getProduct());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "product";
		}
		return "redirect:productManager";
	}

	@RequestMapping(value = { "admin/deleteProduct" }, method = RequestMethod.GET)
	public String removeProductHandler(Model model, //
			@RequestParam(value = "code", defaultValue = "") Integer id) {
		if (id != null) {
			productService.deleteReservation(id);
		}
		return "redirect:productManager";
	}

	// GET : Hiển thị Order List
	@RequestMapping(value = { "/admin/orderList" }, method = RequestMethod.GET)
	public String orderList(Model model, @RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		Sort sortable = Sort.by("id").descending();
		Pageable pageable = PageRequest.of(page, size, sortable);
		Page<Order> result = orderService.findAll(pageable);
		System.out.println("Result : " + result.getNumber());
		model.addAttribute("paginationResult", result);
		return "orderList";

	}

	@RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
	public String orderView(Model model, @RequestParam("orderId") Integer orderId) {
		OrderInfo orderInfo = null;
		if (orderId != null) {
			Optional<Order> orderOpt = orderService.findById(orderId);
			if (orderOpt.isPresent()) {
				Order order = orderOpt.get();
//				orderInfo = new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(),order.getAmount(), order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(), order.getCustomerPhone());
				orderInfo = new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(), order.getAmount(),
						order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(),
						order.getCustomerPhone());
			}
		}
		if (orderInfo == null) {
			return "redirect:/admin/orderList";
		}
		List<OrderDetailInfo> listOrderDetailInfo = new ArrayList<>();
		List<OrderDetail> listOrderDetails = orderDetailService.findAllByOrderId(orderId);
		for (OrderDetail orderDetail : listOrderDetails) {
			OrderDetailInfo orderDetailInfo = new OrderDetailInfo(orderDetail.getId(),
					orderDetail.getProduct().getCode(), orderDetail.getProduct().getName(), orderDetail.getQuanity(),
					orderDetail.getPrice(), orderDetail.getAmount());
			listOrderDetailInfo.add(orderDetailInfo);
		}
		orderInfo.setDetails(listOrderDetailInfo);
		model.addAttribute("orderInfo", orderInfo);
		return "order";
	}

	@RequestMapping(value = { "/admin/accountList" }, method = RequestMethod.GET)
	public String listAccountHandler(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "2") int size) {
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Account> pageAccount = accountService.findByAccountName(likeName, pageable);
			pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationUsers", pageAccount);
			return "accountList";
		}
		Pageable pageable = PageRequest.of(currentPage-1, size);
		Page<Account> pageAccount = accountService.findAll(pageable);
		pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationUsers", pageAccount);
		return "accountList";

	}

	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.GET)
	public String account(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectbyid(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "editAccount";
	}
//luu thong tin
	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.POST)
	public String accountSave(Model model, //
			@ModelAttribute("accountForm") @Validated AccountChangeRoleForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "editAccount";
		}
		try {
			accountService.update(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editAccount";
		}

		return "redirect:/admin/accountList";
	}


	
	
	@RequestMapping(value = { "admin/deleteAccount" }, method = RequestMethod.GET)
	public String removeAccountHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		if (id != null) {
			accountService.delete(id);
		}
		return "redirect:accountList";

	}

	// GET: Hiển thị account
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.GET)
	public String addAccount(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AddAccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService1.selectbyid(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AddAccountForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AddAccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm1", accountForm);
		return "addAccount";

	}

	// POST: Save product
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.POST)
	public String addAccountSave(Model model, //
			@ModelAttribute("accountForm1") @Validated AddAccountForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "addAccount";
		}
		try {
			System.out.println(accountForm.getAccount());
			accountService1.add(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "addAccount";
		}

		return "redirect:/admin/accountList";

	}

	@RequestMapping(value = { "/searchProduct/{name}" }, method = RequestMethod.GET)
	public String getProductByName(@PathVariable("name") String Name, @PathVariable("last") String Last,
			ModelMap modelMap) {
		List<Product> student = productService.selectAll();
		modelMap.addAttribute("message", student);
		return "productList";
	}

	// GET : Profile
	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
	public String userInfo(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountForm accountForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountForm = new AccountForm(account);
		model.addAttribute("accountForm", accountForm);
		model.addAttribute("userDetails", userDetails);
		return "accountInfo";
	}

	// POST : Update Profile
	@RequestMapping(value = { "/admin/userUpdate" }, method = RequestMethod.POST)
	public String usertUpdate(Model model, @ModelAttribute("accountForm") @Validated AccountForm accountForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "back-end/accountUpdate";
		}
		try {
			accountService.updateProfile(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountUpdate";
		}
		return "redirect:accountInfo";
	}

	// GET : Changpassword
	@RequestMapping(value = { "/admin/accountChangePassword" }, method = RequestMethod.GET)
	public String userChangePass(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountForm accountForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountForm = new AccountForm(account);
		model.addAttribute("accountForm", accountForm);
		model.addAttribute("userDetails", userDetails);
		return "passwordChange";
	}

	// POST : Update Changepassword
	@RequestMapping(value = { "/admin/accountChangePassword" }, method = RequestMethod.POST)
	public String usertChangePass(Model model, @ModelAttribute("accountForm") @Validated AccountForm accountForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "back-end/accountChangePassword";
		}
		try {
			accountService.updatePassword(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountChangePassword";
		}
		return "redirect:accountInfo";
	}

	@RequestMapping(value = { "/admin/passwordChange" }, method = RequestMethod.GET)
	public String editPass(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountChangePasswordForm accountChangeForm = null;
		Account account = accountService1.findByUserName(userDetails.getUsername());
		accountChangeForm = new AccountChangePasswordForm(account);
		accountChangeForm.setNewPassword(true);
		model.addAttribute("accountChangePasswordForm", accountChangeForm);
		return "passwordChange";
	}

	@RequestMapping(value = { "/admin/passwordChange" }, method = RequestMethod.POST)
	public String savePass(Model model, //
			@ModelAttribute("accountChangePasswordForm") @Validated AccountChangePasswordForm accountChangePasswordForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			return "passwordChange";
		}
		try {

			accountService1.updatePassword(accountChangePasswordForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "passwordChange";
		}
		return "redirect:accountInfo";
	}

	//

	@RequestMapping(value = { "/admin/editInfo" }, method = RequestMethod.GET)
	public String editInfo(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById (id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountForm(account);
			}
		}

		if (accountForm == null) {
			accountForm = new AccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "editInfo";
	}
	@RequestMapping(value = { "/admin/editInfo" }, method = RequestMethod.POST)
	public String saveInfo(Model model, //
			@ModelAttribute("accountForm") @Validated AccountForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "editInfo";
		}
		try {
			System.out.println(accountForm.getAccount());
			accountService.update(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editInfo";
		}

		return "redirect:/admin/accountInfo";
	}


	@RequestMapping(value = { "/accountImage" }, method = RequestMethod.GET)
	public void accountImage(HttpServletRequest request, HttpServletResponse response, Model model,

			@RequestParam("id") Integer id) throws IOException {
		Account account = null;
		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById(id);
			if (accountOpt.isPresent()) {
				account = accountOpt.get();
			}
		}
		if (account != null && account.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(account.getImage());
		}
		response.getOutputStream().close();
	}

}
