package com.dd.tan.demo;

import java.util.ArrayList;

import com.dd.ta.dao.AccessoryDao;
import com.dd.ta.dao.BaseDao;
import com.dd.ta.dao.CategoryDao;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;


public class CategoryDemo {
	private static String tableName;
	static CategoryDao categoryDao = new CategoryDao();

	public static void main(String[] args) {
		insertCategoryTest();
		updateCategoryTest();
//		deleteCategoryTest();
		findAllTest();
		fillByIdTest();
		fillByNameTest();
	}

	public static Integer insertCategoryTest() {
		Category category = new Category(1, "Tan");
		Category category2 = new Category(2, "The Tan");
		Category category3 = new Category(3, "Tan 3");
		int check;
		if (categoryDao.insert(category) == 1 && categoryDao.insert(category2) == 1
				&& categoryDao.insert(category3) == 1) {
			check = 1;
			System.out.println("Insert Success");
		} else {
			check = 1;
			System.out.println("Insert Fail");
		}
		return check;
	}
	
	public static Integer updateCategoryTest() {
		int check;
		ArrayList<BaseRow> objCategory = categoryDao.findAll();
		Category oldCategory = (Category) objCategory.get(2);
		Category newCategory = new Category(4,"Tan Update");
		int i = categoryDao.update(oldCategory, newCategory);
		if(i==1) {
			check = 1;
			System.out.println("Update Success");
		}else {
			check = 1;
			System.out.println("Update Fail");
		}
		return check;
	}
	
	public static boolean deleteCategoryTest() {
		boolean check;
		ArrayList<BaseRow> objCategory = categoryDao.findAll();
		Category category = (Category) objCategory.get(1);
		check = categoryDao.delete(category);
		System.out.println("Delete : "+category+" "+check);
		return check;
	}
	
	public static ArrayList<BaseRow> findAllTest() {
		ArrayList<BaseRow> listCategory = categoryDao.findAll();
		System.out.println(listCategory);
		return listCategory;
	}
	
	public static void fillByIdTest() {
		BaseRow category = categoryDao.findId(2);
		System.out.println(category);
	}
	
	public static void fillByNameTest() {
		BaseRow category = categoryDao.findName("Tan");
		System.out.println(category);
	}


}
