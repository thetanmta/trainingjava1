package com.dd.ta.dao;

import java.util.ArrayList;

import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class CategoryDao extends BaseDao {
	public CategoryDao() {
		super();
		this.tableName="category";
	}
	public Integer insert(Category row) {
		return super.insert(row);
	}
	public Integer update(Category oldRow, Category newRow) {
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Category row) {
		return super.delete(row);
	}
	@Override
	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}
	
	@Override
	public Category findId(int id) {
		return (Category) super.findId(id);
	}
	
	@Override
	public Category findName(String name) {
		return (Category) super.findName(name);
	}

}
