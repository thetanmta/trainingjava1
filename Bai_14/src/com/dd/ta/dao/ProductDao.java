package com.dd.ta.dao;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;

import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Product;

public class ProductDao extends BaseDao{
	public ProductDao() {
		super();
		this.tableName="product";
	}	
	
	public Integer insert(Product row) {
		return super.insert(row);
	}
	public Integer update(Product oldRow, Product newRow) {
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Product row) {
		return super.delete(row);
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}
	
	@Override
	public Product findId(int id) {
		return (Product) super.findId(id);
	}
	
	@Override
	public Product findName(String name) {
		return (Product) super.findName(name);
	}
}
