package com.dd.ta.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Product;

public abstract class BaseDao implements IDao{
	private Database database = Database.getInstance();
	protected String tableName;

	public BaseDao() {
	}
	
	protected Integer insert(BaseRow row) {
		int i;
		i = database.insertTable(tableName, row);
		return i;
	}
	
	protected Integer update(BaseRow oldRow,BaseRow newRow) {
		int i;
		i = database.updateTable(tableName,oldRow , newRow);
		return i;
	}

	protected boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
				boolean check;
				check = database.deleteTable(tableName, row);
				return check;
	}
	@Override
	public ArrayList<BaseRow> findAll(){
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listProducts = database.selectTable(tableName);
		return listProducts;
	}
	@Override
	public BaseRow findId(int id) {
		// TODO Auto-generated method stub
				ArrayList<BaseRow> listProducts =  database.selectTable(tableName);
				BaseRow products = listProducts.stream().filter(product -> id == product.getId()).findAny()
						.orElse(null);
				return products;
	}
	@Override
	public BaseRow findName(String name) {
		// TODO Auto-generated method stub
				ArrayList<BaseRow> listProducts =  database.selectTable(tableName);
				BaseRow products = listProducts.stream().filter(product -> name == product.getName()).findAny()
						.orElse(null);
				return products;
	}
}
