package com.dd.ta.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public ArrayList<BaseRow> products;
	public ArrayList<BaseRow> categories;
	public ArrayList<BaseRow> accessorys;
	private static Database instance;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();

		this.products = new ArrayList<BaseRow>();
		this.categories = new ArrayList<BaseRow>();
		this.accessorys = new ArrayList<BaseRow>();
	}

	public ArrayList<BaseRow> getBaseRow(String name) {
		ArrayList<BaseRow> baseRow = new ArrayList<BaseRow>();
		if (name.equals("product")) {
			baseRow = this.products;
		}
		if (name.equals("category")) {
			baseRow = this.categories;
		}
		if (name.equals("accessory")) {
			baseRow = this.accessorys;
		}
		return baseRow;
	}

	public Integer insertTable(String name, BaseRow row) {
		int i;
		getBaseRow(name).add(row);
		i = 1;
		return i;
	}

	public ArrayList<BaseRow> selectTable(String name) {
		return getBaseRow(name);
	}

	public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
		int i = 0;
		ArrayList<BaseRow> objProduct = selectTable(name);
		Integer oldProduct = objProduct.indexOf(oldRow);
		getBaseRow(name);
		i = 1;
		return i;
	}

	public boolean deleteTable(String name, BaseRow row) {
		boolean check = false;
		check = getBaseRow(name).remove(row);
		return check;
	}

	public void truncateTable(String name) {
		getBaseRow(name).clear();
		System.out.println("Truncate product");

	}

	public Integer updateTable(String name, int id, BaseRow row) {
		int check = 0;
		ArrayList<BaseRow> listProducts = selectTable(name);
		BaseRow product = listProducts.stream().filter(products -> id == products.getId()).findAny().orElse(null);
		Integer index = listProducts.indexOf(product);
		getBaseRow(name).set(index, row);
		check = 1;
		return check;
	}

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}
}
