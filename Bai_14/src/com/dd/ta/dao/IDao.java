package com.dd.ta.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dd.tan.entity.BaseRow;

public interface IDao {
	public abstract ArrayList<BaseRow> findAll();
	public abstract BaseRow findId(int id);
	public abstract BaseRow findName(String name);
}
