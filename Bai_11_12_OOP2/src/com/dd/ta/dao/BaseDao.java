package com.dd.ta.dao;

import java.util.ArrayList;

import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Product;

public abstract class BaseDao {
	static Database database = new Database();
	protected String tableName;

	public BaseDao() {
	}
	
	protected Integer insert(BaseRow row) {
		int i;
		i = database.insertTable(tableName, row);
		return i;
	}
	
	protected Integer update(BaseRow oldRow,BaseRow newRow) {
		int i;
		i = database.updateTable(tableName,oldRow , newRow);
		return i;
	}
	protected boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
				boolean check;
				check = database.deleteTable(tableName, row);
				return check;
	}
	protected ArrayList<BaseRow> findAll(){
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listProducts = database.selectTable(tableName);
		return listProducts;
	}
	protected BaseRow findById(int id) {
		// TODO Auto-generated method stub
				ArrayList<BaseRow> listProducts =  database.selectTable(tableName);
				BaseRow products = listProducts.stream().filter(product -> id == product.getId()).findAny()
						.orElse(null);
				return products;
	}
	protected BaseRow findByName(String name) {
		// TODO Auto-generated method stub
				ArrayList<BaseRow> listProducts =  database.selectTable(tableName);
				BaseRow products = listProducts.stream().filter(product -> name == product.getName()).findAny()
						.orElse(null);
				return products;
	}

}
