package com.dd.ta.dao;

import java.util.ArrayList;

import javax.xml.crypto.Data;

import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Product;

public class AccessoryDao extends BaseDao {
	public AccessoryDao() {
		super();
		this.tableName="accessory";
	}
	public Integer insert(Accessory row) {
		return super.insert(row);
	}
	public Integer update(Accessory oldRow, Accessory newRow) {
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Accessory row) {
		return super.delete(row);
	}
	@Override
	public ArrayList<BaseRow> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}
	
	@Override
	public Accessory findById(int id) {
		// TODO Auto-generated method stub
		return (Accessory) super.findById(id);
	}
	
	@Override
	public Accessory findByName(String name) {
		// TODO Auto-generated method stub
		return (Accessory) super.findByName(name);
	}
}
