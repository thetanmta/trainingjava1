package com.dd.ta.dao;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;

import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Product;

public class ProductDao extends BaseDao{
	public ProductDao() {
		super();
		this.tableName="product";
	}	
	
	public Integer insert(Product row) {
		return super.insert(row);
	}
	public Integer update(Product oldRow, Product newRow) {
		// TODO Auto-generated method stub
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Product row) {
		// TODO Auto-generated method stub
		return super.delete(row);
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}
	
	@Override
	public Product findById(int id) {
		// TODO Auto-generated method stub
		return (Product) super.findById(id);
	}
	
	@Override
	public Product findByName(String name) {
		// TODO Auto-generated method stub
		return (Product) super.findByName(name);
	}

}
