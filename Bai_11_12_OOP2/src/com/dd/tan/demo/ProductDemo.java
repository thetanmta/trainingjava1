package com.dd.tan.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import com.dd.ta.dao.BaseDao;
import com.dd.ta.dao.Database;
import com.dd.ta.dao.ProductDao;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class ProductDemo {
private static String tableName;
static ProductDao productDao = new ProductDao();
	
	public static void main(String[] args) {
		insertProductTest();
		updateProductTest();
		deleteProductTest();
		findAllProductTest();
		findByIdTest();
		findByNameTest();
	}
	
	public static Integer insertProductTest() {
		Product product = new Product(1,"Tan");
		Product product2 = new Product(2,"Tan 1");
		Product product3 = new Product(3,"tan 2");
		BaseRow product4 = new Product(4, "tan 3"); 
		int check;
		if(productDao.insert(product)==1&&productDao.insert(product2)==1 &&productDao.insert(product3)==1) {
			check=1;
			System.out.println("Insert Product Success");
		}else {
			check=0;
			System.out.println("Insert Product Fail");
		}
		return check;
	}
	
	public static Integer updateProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		Product oldProduct = (Product) listProducts.get(1);
		Product newProduct = new Product(4,"newUpdate");
		int i = productDao.update( oldProduct, newProduct);
		if(i==1) {
			System.out.println("Update Product Pass");
		}else {
			i = 0;
			System.out.println("Update Product Fail");
		}
		return i;
	}
	
	public static boolean deleteProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		Product product = (Product) listProducts.get(2);
		boolean check = productDao.delete( product);
		System.out.println("Delete : "+product+" "+check);
		return check;
	}
	
	public static void findAllProductTest() {
		ArrayList<BaseRow> listProducts = productDao.findAll();
		System.out.println(listProducts);
	}
	
	public static void findByIdTest() {
		BaseRow product = productDao.findById(4);
		System.out.println(product);
	}
	
	public static void findByNameTest() {
		BaseRow product = productDao.findByName("Tan");
		System.out.println(product);
	}

}
