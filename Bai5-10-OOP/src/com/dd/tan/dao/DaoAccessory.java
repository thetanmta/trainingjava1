package com.dd.tan.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import com.dd.tan.entity.Accessory;

public class DaoAccessory extends BaseDao{
	static Database dataBase = new Database();

	@Override
	public Integer insert(Object row) {
		// TODO Auto-generated method stub
		int i = dataBase.insertTable("accessory", row);
		return i;
	}

	@Override
	public Integer update(Object oldRow, Object newRow) {
		// TODO Auto-generated method stub
		int i = dataBase.updateTable("accessory", oldRow, newRow);
		return i;
	}

	@Override
	public boolean delete(Object row) {
		// TODO Auto-generated method stub
		boolean check = dataBase.deleteTable("accessory", row);
		return check;
	}

	@Override
	public ArrayList<Object> findAll() {
		// TODO Auto-generated method stub
		ArrayList<Object> listAccessorys = dataBase.selectTable("accessory");
		return listAccessorys;
	}

	@Override
	public Object findById(int id) {
		// TODO Auto-generated method stub
		ArrayList<Accessory> listAccessorys = (ArrayList) dataBase.selectTable("accessory");
		Accessory accessorys = listAccessorys.stream().filter(accessory -> id == accessory.getId()).findAny()
				.orElse(null);
		return accessorys;
	}

	@Override
	public Object findByName(String name) {
		// TODO Auto-generated method stub
		ArrayList<Accessory> listAccessorys = (ArrayList) dataBase.selectTable("accessory");
		Accessory accessorys = listAccessorys.stream().filter(accessory -> name.equals(accessory.getName())).findAny()
				.orElse(null);
		return accessorys;
	}
	
//	public static Integer insert(Object row) {
//		int i = dataBase.insertTable("accessory", row);
//		return i;
//	}
//	
//	public static Integer update(Object oldRow,Object newRow) {
//		int i = dataBase.updateTable("accessory", oldRow, newRow);
//		return i;
//	}
//	
//	public static boolean delete(Object row) {
//		boolean check = dataBase.deleteTable("accessory", row);
//		return check;
//	}
//	
//	public static ArrayList<Object> findAll(){
//		ArrayList<Object> listAccessorys = dataBase.selectTable("accessory");
//		return listAccessorys;
//	}
//	
//	public static Object findById(int id) {
//		ArrayList<Object> listAccessorys = dataBase.selectTable("accessory");
//		Accessory accessory1 = new Accessory();
//		for (int i = 0; i < listAccessorys.size(); i++) {
//			Accessory accessory2 = (Accessory) listAccessorys.get(i);
//			if (id == accessory2.getId()) {
//				accessory1 = accessory2;
//			}
//		}
//		return accessory1;
//	}
//	public static Object findByName(String name) {
//		ArrayList<Object> listAccessorys = dataBase.selectTable("accessory");
//		Accessory accessory1 = new Accessory();
//		for (int i = 0; i < listAccessorys.size(); i++) {
//			Accessory accessory2 = (Accessory) listAccessorys.get(i);
//			if (name.equals(accessory2.getName())) {
//				accessory1 = accessory2;
//			}
//		}
//		return accessory1;
//	}

}
