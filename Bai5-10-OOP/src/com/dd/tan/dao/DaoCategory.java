package com.dd.tan.dao;

import java.util.ArrayList;

import com.dd.tan.entity.Category;

public class DaoCategory extends BaseDao{
	static Database database = new Database();
	public Integer insert(Object row) {
		// TODO Auto-generated method stub
		int i;
		i = database.insertTable("category", row);
		return i;
	}


	public Integer update(Object oldRow, Object newRow) {
		// TODO Auto-generated method stub
		int i;
		i = database.updateTable("category", oldRow, newRow);
		return i;
	}

	
	public boolean delete(Object row) {
		// TODO Auto-generated method stub
		boolean check;
		check = database.deleteTable("category", row);
		return check;
	}

	@Override
	public ArrayList<Object> findAll() {
		// TODO Auto-generated method stub
		ArrayList<Object> listCategory = database.selectTable("category");
		return listCategory;
	}

	@Override
	public Object findById(int id) {
		// TODO Auto-generated method stub
		ArrayList<Category> listCategorys = (ArrayList) database.selectTable("category");
		Category categorys = listCategorys.stream().filter(category -> id == category.getId()).findAny()
				.orElse(null);
		return categorys;
	}

	@Override
	public Object findByName(String name) {
		// TODO Auto-generated method stub
		ArrayList<Category> listCategorys = (ArrayList) database.selectTable("category");
		Category categorys = listCategorys.stream().filter(category -> name == category.getName()).findAny()
				.orElse(null);
		return categorys;
	};

}
