package com.dd.tan.dao;

import java.util.ArrayList;

public abstract class BaseDao {
	public abstract Integer insert(Object row);
	public abstract Integer update(Object oldRow,Object newRow);
	public abstract boolean delete(Object row);
	public abstract ArrayList<Object> findAll();
	public abstract Object findById(int id);
	public abstract Object findByName(String name);
	public Integer insertTable(Object row) {
		// TODO Auto-generated method stub
		return null;
	}

}
