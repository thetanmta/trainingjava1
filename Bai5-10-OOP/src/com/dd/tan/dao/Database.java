package com.dd.tan.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public Database Instances;
	private ArrayList<Product> products;
	private ArrayList<Category> categories;
	private ArrayList<Accessory> accessorys;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();

		this.products = new ArrayList<Product>();
		this.categories = new ArrayList<Category>();
		this.accessorys = new ArrayList<Accessory>();

	}

	public int insertTable(String name, Object row) {
		int i = 0;
		if (name.equals("product")) {
			this.productTable.add(row);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.add(row);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.add(row);
			i = 1;
		}
		return i;
	}
	
	public Integer updateTable(String name, Object oldRow, Object newRow) {
		int i = 0;
		if (name.equals("product")) {
			ArrayList<Object> objProduct = selectTable("product");
			Integer oldProduct = objProduct.indexOf(oldRow);
			this.productTable.set(oldProduct, newRow);
			i = 1;
		}
		if (name.equals("category")) {
			ArrayList<Object> objCategory = selectTable("category");
			Integer oldCategory = objCategory.indexOf(oldRow);
			this.categoryTable.set(oldCategory, newRow);
			i = 1;
		}
		if (name.equals("accessory")) {
			ArrayList<Object> objAccessory = selectTable("accessory");
			Integer oldAccessory = objAccessory.indexOf(oldRow);
			this.accessoryTable.set(oldAccessory, newRow);
			i = 1;
		}
		return i;
	}

	public ArrayList<Object> selectTable(String name) {

		if (name.equals("product")) {
			return this.productTable;
		}
		if (name.equals("category")) {
			return this.categoryTable;

		}
		if (name.equals("accessory")) {
			return this.accessoryTable;

		}
		return null;
	}

	// delete
	public Boolean deleteTable(String name, Object row) {
		Boolean check = true;
		if (name.equals("product")) {
			this.productTable.remove(row);
		}
		if (name.equals("category")) {
			return this.categoryTable.remove(row);
		}
		if (name.equals("accessory")) {
			this.accessoryTable.remove(row);

		}
		return check;
	}

	//
	public void truncateTable(String name) {

		if (name.equals("product")) {
			this.productTable.clear();
			System.out.println("TrucateProduct");
		}
		if (name.equals("category")) {
			this.productTable.clear();
			System.out.println("TrucateCategory");
		}
		if (name.equals("accessory")) {
			this.productTable.clear();
			System.out.println("TrucateAccessory");

		}

	}

	public Integer update(String name, int id, Object row) {
		int i = 0;
		if (name.equals("product")) {
			ArrayList<Product> listProduct = (ArrayList) selectTable("procduct");
			Product product = listProduct.stream().filter(products -> id == ((Product) products).getId()).findAny()
					.orElse(null);

			Integer index = listProduct.indexOf(product);
			this.productTable.set(index, row);
			i = 1;

		}
		if (name.equals("category")) {
			// Dng stream
			ArrayList<Category> listCategorys = (ArrayList) selectTable("category");
			Category category = listCategorys.stream().filter(categorys -> id == categorys.getId()).findAny()
					.orElse(null);
			Integer index = listCategorys.indexOf(category);
			this.categoryTable.set(index, row);
			i = 1;
			// Khng dung stream
//			ArrayList<Object> objCategory = selectTable("category");
//			for (int i = 0; i < objCategory.size(); i++) {
//				Category category2 = (Category) objCategory.get(i);
//				if (id == category2.getId()) {
//					Integer oldIndexCategory = objCategory.indexOf(category2);
//					this.categoryTable.set(oldIndexCategory, row);
//					check = 1;
//				}
//			}
		}
		if (name.equals("accessory")) {
			// Dng stream
			ArrayList<Accessory> listAccessorys = (ArrayList) selectTable("accessory");
			Accessory accessorys = listAccessorys.stream().filter(accessory -> id == accessory.getId()).findAny()
					.orElse(null);
			Integer index = listAccessorys.indexOf(accessorys);
			this.accessoryTable.set(index, row);
			i = 1;
			// Khng dng stream
//			ArrayList<Object> objAccessory = selectTable("accessory");
//			for (int i = 0; i < objAccessory.size(); i++) {
//				Accessory accessory2 = (Accessory) objAccessory.get(i);
//				if (id == accessory2.getId()) {
//					Integer oldIndexAccessory = objAccessory.indexOf(accessory2);
//					this.accessoryTable.set(oldIndexAccessory, row);
//					check = 1;
//				}
//			}
		}

		return i;
	}
}
