package com.dd.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import com.dd.tan.dao.Database;
import com.dd.tan.entity.Product;

public class ProductDemo {
	static Database dataBase = new Database();

	public static void main(String[] args) {
		createProductTest();
		selectProductTest();
		printProduct();
	}

	public static void createProductTest() {
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		Product product = new Product(1, "Ca phe", 2, hientai, 10, true);
		Product product2 = new Product(2, "Ca phe den", 2, tuansau, 4, true);
		Product product3 = new Product(3, "Ca phe sua", 2, tuansau, 2, false);
		Product product4 = new Product(4, "Coca cola", 3, tuantruoc, 0, true);
		Product product5 = new Product(5, "Pepsi", 3, tuantruoc, 2, false);
		dataBase.insertTable("product", product);
		dataBase.insertTable("product", product2);
		dataBase.insertTable("product", product3);
		dataBase.insertTable("product", product4);
		dataBase.insertTable("product", product5);
	}

	public static void selectProductTest() {
		ArrayList<Object> products = dataBase.selectTable("product");
		System.out.println(products);
	}

	public static void printProduct() {
		LocalDate hientai = LocalDate.now();
		System.out.println(new Product(1, "Ca phe", 2, hientai, 10, true));
	}

}
