package com.dd.demo;

import java.util.ArrayList;

import com.dd.tan.dao.Database;
import com.dd.tan.entity.Category;

public class CategoryDemo {
	static Database dataBase = new Database();
	public static void main(String[] args) {
		createCategoryTest();
		selectCategoryTest();
		printCategory();
	}
	
	public static void createCategoryTest(){
		Category category1 = new Category(1,"Giat Khat");
		Category category2 = new Category(2,"Loai co ga");
		Category category3 = new Category(4,"Loai Sua");
		Category category4 = new Category(5,"Bia");
		Category category5 = new Category(6,"Ruou");
		dataBase.insertTable("category", category1);
		dataBase.insertTable("category", category2);
		dataBase.insertTable("category", category3);
		dataBase.insertTable("category", category4);
		dataBase.insertTable("category", category5);
	}
	
	public static void selectCategoryTest() {
		ArrayList<Object> categorys = dataBase.selectTable("category");
		System.out.println(categorys);
	}
	
	private static void printCategory() {
		System.out.println( new Category(1,"Giat Khat"));
	}


}
