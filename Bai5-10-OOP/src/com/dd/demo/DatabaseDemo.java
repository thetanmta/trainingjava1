package com.dd.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;
import com.dd.tan.dao.Database;
import com.dd.tan.entity.Product;

public class DatabaseDemo {
	static Database database = new Database();

	public static void main(String[] args) {
		insertTableTest();
		System.out.println("select Table");
		selectTableTest();
		System.out.println("update table");
		updateTableTest();
		System.out.println("delete table");
		deleteTableTest();
		System.out.println("truncte Table");
		truncateTableTest();
		// initDatabase();
		System.out.println("in ra tất cả cac bảng");
		printTableTest();

	}

	private static void insertTableTest() {

		Product product = new Product(2, "the tan");
		database.insertTable("product", product);
		Product product2 = new Product(3, "the tan");
		database.insertTable("product", product2);
		Category category = new Category(1, "category");
		database.insertTable("category", category);
		Category category1 = new Category(2, "category");
		database.insertTable("category", category);
		Accessory accessory = new Accessory(1, "accessory");
		database.insertTable("accessory", accessory);

	}

	private static void selectTableTest() {
		ArrayList<Object> products = database.selectTable("product");
		System.out.println(products);
		ArrayList<Object> categorys = database.selectTable("category");
		System.out.println(categorys);
		ArrayList<Object> accessorys = database.selectTable("accessory");
		System.out.println(accessorys);

	}

	private static void deleteTableTest() {
		ArrayList<Object> products = database.selectTable("product");
		products.remove(0);
		System.out.println(products);
	}

	private static void truncateTableTest() {
		database.truncateTable("categoryTable");
	}

	private static void initDatabase() {
		// Tao list Product
		ArrayList<Product> product = new ArrayList<Product>();
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		try {
			product.add(new Product(1, "Ca phe", 2, hientai, 10, true));
			product.add(new Product(2, "Ca phe den", 2, tuansau, 4, true));
			product.add(new Product(3, "Ca phe sua", 2, tuansau, 2, false));
			product.add(new Product(4, "Coca cola", 3, tuansau, 0, true));
			product.add(new Product(5, "Pepsi", 3, hientai, 2, false));
			product.add(new Product(6, "7up", 3, hientai, 3, true));
			product.add(new Product(7, "Banh ngot", 4, tuantruoc, 4, false));
			product.add(new Product(8, "Banh kem", 4, tuantruoc, 8, true));
			product.add(new Product(9, "Tra sua", 1, tuantruoc, 0, false));
			product.add(new Product(10, "Kem", 2, hientai, 0, true));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		// Tao list category
		ArrayList<Category> category = new ArrayList<Category>();
		try {
			category.add(new Category(1, "Category1"));
			category.add(new Category(2, "Category2"));
			category.add(new Category(3, "Category3"));
			category.add(new Category(4, "Category4"));
			category.add(new Category(5, "Category5"));
			category.add(new Category(6, "Category6"));
			category.add(new Category(7, "Category7"));
			category.add(new Category(8, "Category8"));
			category.add(new Category(9, "Category9"));
			category.add(new Category(10, "Category10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		// Tao list accessory
		ArrayList<Accessory> accessory = new ArrayList<Accessory>();
		try {
			accessory.add(new Accessory(1, "Accessory1"));
			accessory.add(new Accessory(2, "Accessory2"));
			accessory.add(new Accessory(3, "Accessory3"));
			accessory.add(new Accessory(4, "Accessory4"));
			accessory.add(new Accessory(5, "Accessory5"));
			accessory.add(new Accessory(6, "Accessory6"));
			accessory.add(new Accessory(7, "Accessory7"));
			accessory.add(new Accessory(8, "Accessory8"));
			accessory.add(new Accessory(9, "Accessory9"));
			accessory.add(new Accessory(10, "Accessory10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		database.insertTable("product", product);
		database.insertTable("category", category);
		database.insertTable("accessory", accessory);

	}

	private static void printTableTest() {
		System.out.println("Print Table product: ");
		ArrayList<Object> products = database.selectTable("product");
		System.out.println(products);
		System.out.println("Print Table category: ");
		ArrayList<Object> categorys = database.selectTable("category");
		System.out.println(categorys);
		System.out.println("Print Table accessory: ");
		ArrayList<Object> accessorys = database.selectTable("accessory");
		System.out.println(accessorys);
	}

	private static void updateTableTest() {
		// update table product
		ArrayList<Object> objProduct = database.selectTable("product");
		Object oldProduct = objProduct.get(1);
		Object newProduct = new Product(2, "UpdateProduct");
		database.updateTable("product", oldProduct, newProduct);
		System.out.println("Old Product : " + oldProduct + "-->" + "New product : " + newProduct);

		// update table category
		ArrayList<Object> objCategory = database.selectTable("category");
		Object oldCategory = objCategory.get(0);
		Object newCategory = new Product(2, "UpdateCategoty");
		database.updateTable("category", oldCategory, newCategory);
		System.out.println("Old Product : " + oldCategory + "-->" + "New Category : " + newCategory);

		//update table accessory
		ArrayList<Object> objAccessory = database.selectTable("accessory");
		Object oldAccessory = objAccessory.get(0);
		Object newAccessory = new Product(2,"Updateaccessory");
		database.updateTable("accessory", oldAccessory,  newAccessory);
		System.out.println("Old Product : "+oldAccessory+"-->"+"New Accessory : "+newAccessory);
	}
}
