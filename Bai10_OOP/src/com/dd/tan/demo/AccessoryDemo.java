package com.dd.tan.demo;

import java.util.ArrayList;

import com.dd.ta.dao.AccessoryDao;
import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.BaseRow;

public class AccessoryDemo {
	static AccessoryDao accessoryDao = new AccessoryDao();
	public static void main(String[] args) {
		insertAccessory();
		System.out.println("update");
		updateTest();
		//System.out.println("delete");
		//deleteTest();
		System.out.println("");
		findAllTest();
		findByIdTest();
		findByNameTest();
		
	}
	public static Integer insertAccessory() {
		int i;
		Accessory accessory = new Accessory(1,"Tan");
		BaseRow accessory2 = new Accessory(2, "Tan 2");
		BaseRow accessory3 = new Accessory(3, "Tan 3");
		if (accessoryDao.insert(accessory) == 1 && accessoryDao.insert(accessory2) == 1
				&& accessoryDao.insert(accessory3) == 1) {
			i = 1;
			System.out.println("Insert Success");
		} else {
			i=0;
			System.out.println("Insert Fail");
		}
		return i;
	}
	public static Integer updateTest() {
		int i;
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		BaseRow oldAccessory = listAccessorys.get(1);
		BaseRow newAccessory = new Accessory(4, "Tan update");
		i = accessoryDao.update(oldAccessory, newAccessory);
		if(i==1) {
			System.out.println("Update Accessory Pass");
		}else {
			i = 0;
			System.out.println("Update Accessory Fail");
		}
		return i;
	}
	
	public static boolean deleteTest() {
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		BaseRow accessory = listAccessorys.get(2);
		boolean check = accessoryDao.delete(accessory);
		System.out.println("Delete : "+accessory+" "+check);
		return check;
	}
	
	public static ArrayList<BaseRow> findAllTest(){
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		System.out.println(listAccessorys);
		return listAccessorys;
	}
	
	public static void findByIdTest() {
		BaseRow accessory = accessoryDao.findById(1);
		System.out.println(accessory);
	}
	
	public static void findByNameTest() {
		BaseRow accessory = accessoryDao.findByName("Tan");
		System.out.println(accessory);
	}

}
