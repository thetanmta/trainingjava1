package com.dd.ta.dao;

import java.util.ArrayList;

import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class CategoryDao extends BaseDao{
	
	static Database database = new Database();

	@Override
	public Integer insert(BaseRow row) {
		int i;
		i = database.insertTable("category", row);
		return i;
	}

	@Override
	public Integer update(BaseRow oldRow, BaseRow newRow) {
		// TODO Auto-generated method stub
		int i;
		i = database.updateTable("category", oldRow, newRow);
		return i;
	}

	@Override
	public boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
		boolean check;
		check = database.deleteTable("category", row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listCategory = database.selectTable("category");
		return listCategory;
	}

	@Override
	public BaseRow findById(int id) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listCategorys =  database.selectTable("category");
		BaseRow categorys = listCategorys.stream().filter(category -> id == category.getId()).findAny()
				.orElse(null);
		return categorys;
	}

	@Override
	public BaseRow findByName(String name) {
		// TODO Auto-generated method stub
		ArrayList<BaseRow> listCategorys = database.selectTable("category");
		BaseRow categorys = listCategorys.stream().filter(category -> name == category.getName()).findAny()
				.orElse(null);
		return categorys;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	

	

	
//	public static Integer insert(Object row) {
//		int i;
//		i = database.insertTable("category", row);
//		return i;
//	}
//
//	public static Integer update(Object oldRow, Object newRow) {
//		int i;
//		i = database.updateTable("category", oldRow, newRow);
//		return i;
//	}
//
//	public static boolean delete(Object row) {
//		boolean check;
//		check = database.deleteTable("category", row);
//		return check;
//	}
//
//	public static ArrayList<Object> findAll() {
//		ArrayList<Object> listCategory = database.selectTable("category");
//		return listCategory;
//	}
//
//	public static Object findById(int id) {
//		ArrayList<Object> listCategory = database.selectTable("category");
//		Category category1 = new Category();
//		for(int i=0;i<listCategory.size();i++) {
//			Category category2 = (Category) listCategory.get(i);
//			if(id==category2.getId()) {
//				category1 = category2;
//			}
//		}
//		return category1;
//	}
//	public static Object findByName(String name) {
//		ArrayList<Object> listCategorys = dataBase.selectTable("category");
//		Category category1 = new Category();
//		for (int i = 0; i < listCategorys.size(); i++) {
//			Category category2 = (AccessoryDao) listCategorys.get(i);
//			if (name.equals(category2.getName())) {
//				category1 = category2;
//			}
//		}
//		return category1;
//	}

}
