package com.dd.ta.dao;

import java.util.ArrayList;

import javax.xml.crypto.Data;

import com.dd.tan.entity.BaseRow;

public class AccessoryDao extends BaseDao {
	static Database database = new Database();

	@Override
	public Integer insert(BaseRow row) {
		int i;
		i = database.insertTable("accessory", row);
		return i;
	}

	@Override
	public Integer update(BaseRow oldRow, BaseRow newRow) {
		// TODO Auto-generated method stub
		int i;
		i = database.updateTable("accessory", oldRow, newRow);
		return i;
	}

	@Override
	public boolean delete(BaseRow row) {
		// TODO Auto-generated method stub
		boolean check;
		check = database.deleteTable("accessory", row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		ArrayList<BaseRow> listProducts = database.selectTable("accessory");
		return listProducts;
	}

	@Override
	public BaseRow findById(int id) {
		ArrayList<BaseRow> listProducts = database.selectTable("accessory");
		BaseRow products = listProducts.stream().filter(product -> id == product.getId()).findAny().orElse(null);
		return products;
	}

	@Override
	public BaseRow findByName(String name) {
		ArrayList<BaseRow> listProducts = database.selectTable("accessory");
		BaseRow products = listProducts.stream().filter(product -> name == product.getName()).findAny().orElse(null);
		return products;
	}

}
