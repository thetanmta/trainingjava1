package com.dd.ta.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dd.tan.entity.Accessory;
import com.dd.tan.entity.BaseRow;
import com.dd.tan.entity.Category;
import com.dd.tan.entity.Product;

public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public ArrayList<BaseRow> products;
	public ArrayList<BaseRow> categories;
	public ArrayList<BaseRow> accessorys;
	public Database instants;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();
		
		this.products = new ArrayList<BaseRow>();
		this.categories = new ArrayList<BaseRow>();
		this.accessorys = new ArrayList<BaseRow>();
	}
	// Object la product , category or accessory
	public Integer insertTable(String name, BaseRow row) {
		int i = 0;
		if (name.equals("product")) {
			this.products.add((Product) row);
			i = 1;
		}
		if (name.equals("category")) {
			this.categories.add((Category) row);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessorys.add((Accessory) row);
			i = 1;
		}
		return i;
	}

	public ArrayList<BaseRow> selectTable(String name) {	
		if (name.equals("product")) {
			return this.products;
		}
		if (name.equals("category")) {
			return this.categories;
		}
		if (name.equals("accessory")) {
			return this.accessorys;
		}
		return null;
	}

	public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
		int i = 0;
		if (name.equals("product")) {
			ArrayList<BaseRow> objProduct = selectTable("product");
			Integer oldProduct = objProduct.indexOf(oldRow);
			this.products.set(oldProduct, newRow);
			i = 1;
		}
		if (name.equals("category")) {
			ArrayList<BaseRow> objCategory = selectTable("category");
			Integer oldCategory = objCategory.indexOf(oldRow);
			this.categories.set(oldCategory, newRow);
			i = 1;
		}
		if (name.equals("accessory")) {
			ArrayList<BaseRow> objAccessory = selectTable("accessory");
			Integer oldAccessory = objAccessory.indexOf(oldRow);
			this.accessorys.set(oldAccessory, newRow);
			i = 1;
		}
		return i;
	}

	public boolean deleteTable(String name, BaseRow row) {
		boolean check = false;
		if (name.equals("product")) {
			check = this.products.remove(row);
		}
		if (name.equals("category")) {
			check = this.categories.remove(row);
		}
		if (name.equals("accessory")) {
			check = this.accessorys.remove(row);
		}
		return check;
	}

	public void truncateTable(String name) {
		if (name.equals("product")) {
			this.products.clear();
			System.out.println("Truncate product");
		}
		if (name.equals("category")) {
			this.categories.clear();
			System.out.println("Truncate category");
		}
		if (name.equals("accessory")) {
			this.accessorys.clear();
			System.out.println("Truncate accessory");
		}
	}

	public Integer updateTable(String name, int id, BaseRow row) {
		int check = 0;
		if (name.equals("product")) {
			//Dng stream
			ArrayList<BaseRow> listProducts =  selectTable("product");
			BaseRow product = listProducts.stream().filter(products -> id == products.getId()).findAny()
					.orElse(null);
			Integer index = listProducts.indexOf(product);
			this.products.set(index, row);
			check = 1;
			//Khng dng stream
//			ArrayList<Object> objProduct = selectTable("product");
//			for (int i = 0; i < objProduct.size(); i++) {
//				Product product2 = (Product) objProduct.get(i);
//				if (id == product2.getId()) {
//					Integer oldIndexProduct = objProduct.indexOf(product2);
//					this.productTable.set(oldIndexProduct, row);
//					check = 1;
//				}
//			}
		}
		if (name.equals("category")) {
			//Dng stream
			ArrayList<BaseRow> listCategorys =  selectTable("category");
			BaseRow category = listCategorys.stream().filter(categorys -> id == categorys.getId()).findAny()
					.orElse(null);
			Integer index = listCategorys.indexOf(category);
			this.categories.set(index, row);
			check = 1;
			// Khng dng stream
//			ArrayList<Object> objCategory = selectTable("category");
//			for (int i = 0; i < objCategory.size(); i++) {
//				Category category2 = (Category) objCategory.get(i);
//				if (id == category2.getId()) {
//					Integer oldIndexCategory = objCategory.indexOf(category2);
//					this.categoryTable.set(oldIndexCategory, row);
//					check = 1;
//				}
//			}
		}
		if (name.equals("accessory")) {
			//Dng stream
			ArrayList<BaseRow> listAccessorys = selectTable("accessory");
			BaseRow accessorys = listAccessorys.stream().filter(accessory -> id == accessory.getId()).findAny()
					.orElse(null);
			Integer index = listAccessorys.indexOf(accessorys);
			this.accessorys.set(index, row);
			check = 1;
			//Khng dng stream
//			ArrayList<Object> objAccessory = selectTable("accessory");
//			for (int i = 0; i < objAccessory.size(); i++) {
//				AccessoryDao accessory2 = (AccessoryDao) objAccessory.get(i);
//				if (id == accessory2.getId()) {
//					Integer oldIndexAccessory = objAccessory.indexOf(accessory2);
//					this.accessoryTable.set(oldIndexAccessory, row);
//					check = 1;
//				}
//			}
		}
		return check;
	}

}
